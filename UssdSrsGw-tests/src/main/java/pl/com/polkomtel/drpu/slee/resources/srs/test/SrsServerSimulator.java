package pl.com.polkomtel.drpu.slee.resources.srs.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

/**
 * <p>SrsServerSimulator is expected to be launched with the following command line arguments:
 * 
 * <p><pre> java &lt;jar-file-name> [-host &lt;host name or IP address>] [-port &lt;port>]</pre>
 * 
 * <p>If no cmd-args are provided explicitly, default ones are assumed, i.e. {@link SrsServerSimulator#DEFAULT_PORT} and {@link SrsServerSimulator#DEFAULT_HOST}.
 * 
 * @author adam.tylman
 */
public class SrsServerSimulator {
	
	private static final Logger logger = Logger.getLogger(SrsServerSimulator.class);
	
	private static final String PORT_CMD_ARG_NAME = "-port";
	private static final String HOST_CMD_ARG_NAME = "-host";
	
	private static final int DEFAULT_PORT = 55000;
	private static final String DEFAULT_HOST = "127.0.0.1";
	
	private static void initServer(String host, int port) {
		try {
			Properties props = new Properties();
			props.setProperty("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
			props.setProperty("org.omg.CORBA.ORBSingletonClass", "org.jacorb.orb.ORBSingleton");
			props.setProperty("org.omg.PortableInterceptor.ORBInitializerClass.bidir_init", "org.jacorb.orb.giop.BiDirConnectionInitializer");
			props.setProperty("OAIAddr", host);
			props.setProperty("OAPort", String.valueOf(port));
			
			try (StringWriter writer = new StringWriter()) {
				props.list(new PrintWriter(writer));
				logger.info("ORB with the following props is going to be initialized:\n" + writer.getBuffer().toString());
			}
			
			SrsServerImpl serverImpl = new SrsServerImpl();
			
			ORB orb = ORB.init((String[]) null, props);
			POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPOA.activate_object(serverImpl);
			rootPOA.the_POAManager().activate();

			org.omg.CORBA.Object serverObj = rootPOA.servant_to_reference(serverImpl);
			String ref = orb.object_to_string(serverObj);
			
			org.jacorb.orb.ORB jorb = (org.jacorb.orb.ORB) orb;
			jorb.addObjectKey(SrsServerSimulator.class.getSimpleName(), ref);
			jorb.run();
		} catch (Exception e) {
			logger.warn("SrsServerSimulator:initServer unable to init!!");
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args) {
		int port = DEFAULT_PORT;
		String host = DEFAULT_HOST;
		
		if(args.length>=2) {
			String arg = null;
			Iterator<String> iter = new LinkedList<String>(Arrays.asList(args)).iterator();
			while(iter.hasNext()) {
				arg = iter.next();
				if(arg.equals(PORT_CMD_ARG_NAME) && iter.hasNext())
					port = Integer.parseInt(iter.next());
				else if(arg.equals(HOST_CMD_ARG_NAME) && iter.hasNext())
					host = iter.next();
			}
		}
		
		initServer(host, port);
	}
}
