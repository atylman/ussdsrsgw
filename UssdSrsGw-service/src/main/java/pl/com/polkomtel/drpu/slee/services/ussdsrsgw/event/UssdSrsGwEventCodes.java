package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventFieldAnnotationAware;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.annotations.EventFields;

import static pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event.UssdSrsGwEventHelper.*;

/**
 * Enumeration used to assign unique event identifiers to service generated events.
 *
 * @author adam.tylman
 *
 */
public enum UssdSrsGwEventCodes implements EventCode, EventFieldAnnotationAware {

	@EventFields(defaultFields = true)
	PROCESSING_INIT(7000),
	
	@EventFields(defaultFields = true, fields = {USSD_CODE, RECHARGED_MSISDN, VOUCHER})
    SRS_REQUEST_SENT(7001),
    
    @EventFields(defaultFields = true, fields = {USSD_CODE, RECHARGED_MSISDN, VOUCHER, SRS_RESP_CODE})
    SRS_RESPONSE_OK(7002),
    
    @EventFields(defaultFields = true, fields = {USSD_CODE, RECHARGED_MSISDN, VOUCHER, SRS_RESP_CODE})
    PROCESSING_SUCCESSFULLY_ENDED(7003),
    
    @EventFields(defaultFields = true, fields = {USSD_CODE, RECHARGED_MSISDN, VOUCHER, SRS_RESP_CODE, ADDITIONAL_INFO})
    SRS_RESPONSE_NOK(7004),
    
    @EventFields(defaultFields = true, fields = {USSD_CODE, RECHARGED_MSISDN, VOUCHER, ADDITIONAL_INFO})
    SRS_COMMUNICATION_FAILURE(7005),
    
    @EventFields(defaultFields = true)
    USSD_TIMEOUT(7006),
    
    @EventFields(defaultFields = true, fields = {ADDITIONAL_INFO})
    NUMBER_NORMALIZER_NGIN_ERROR(7007),
    
    @EventFields(defaultFields = true, fields = {ADDITIONAL_INFO})
    DB_ACCESS_NGIN_ERROR(7008),
    
    @EventFields(defaultFields = true, fields = {ADDITIONAL_INFO})
    INTERNAL_SERVICE_ERROR(7009);

    private final int eventCode;

    private UssdSrsGwEventCodes(final int eventCode) {
        this.eventCode = eventCode;
    }

    /**
     * Return an event code value
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode#getEventCode()
     */
    @Override
    public int getEventCode() {
        return eventCode;
    }

    /**
     * Returns the name of the current enum constant 
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventFieldAnnotationAware#getName()
     */
	@Override
	public String getName() {
		return this.name();
	}
}
