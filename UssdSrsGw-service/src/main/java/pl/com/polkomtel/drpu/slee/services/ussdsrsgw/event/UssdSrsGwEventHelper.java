package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event;

import org.apache.commons.lang.StringUtils;

import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGw;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.GeneralUtils;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventHelper;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.annotations.EventMethod;

/**
 * A factory class to be used for events creation.
 *
 * @author adam.tylman
 *
 */
public class UssdSrsGwEventHelper extends EventHelper {
	
	public static final String ORIG_MSISDN = "ORIG_MSISDN";
	public static final String TIMESTAMP = "TIMESTAMP";
	public static final String TCAP_OID = "TCAP_OID";
	public static final String USSD_CODE = "USSD_CODE";
	public static final String RECHARGED_MSISDN = "RECHARGED_MSISDN";
	public static final String VOUCHER = "VOUCHER";
	public static final String SRS_RESP_CODE = "SRS_RESP_CODE";
	public static final String ADDITIONAL_INFO = "ADDITIONAL_INFO";
	
	public static final String GENERAL_ERROR = "USSD msg processing error";
	private static final String FIELD_UNKNOWN_PLACEHOLDER = "UNKNOWN";
	
	private final UssdSrsGw sbb;
	
	/**
	 * Default constructor
	 * @param sbb a reference to SBB that holds all necessary data
     */
	public UssdSrsGwEventHelper(final UssdSrsGw sbb) {
		this.sbb = sbb;
	}
	
	/**
	 * Holds default event field descriptions to avoid declaring many times the same events
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventHelper#getDefaultEventFields()
     */
	@Override
	protected String[] getDefaultEventFields() {
		return new String[] { ORIG_MSISDN, TIMESTAMP, TCAP_OID };
	}
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = USSD_CODE)
    public String getUssdCode() {
        return sbb.getUssdCode();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = ORIG_MSISDN)
    public String getOriginMsisdn() {
		if(StringUtils.isNotBlank(sbb.getOriginMsisdn()))
			return sbb.getOriginMsisdn();
		else
			return FIELD_UNKNOWN_PLACEHOLDER;
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = TIMESTAMP)
    public String getTimestamp() {
        return GeneralUtils.dateMillisToString(sbb.getTimestamp());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = RECHARGED_MSISDN)
    public String getRechargedMsisdn() {
        return sbb.getRechargedMsisdn();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = VOUCHER)
    public String getVoucher() {
        return sbb.getVoucherId();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = TCAP_OID)
    public String getDialogId() {
        return sbb.getTransactionId();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = SRS_RESP_CODE)
    public String getSrsRespCode() {
        return Integer.toString(sbb.getSrsRespCode());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = ADDITIONAL_INFO)
    public String getAdditionalInfo() {
        return sbb.getAdditionalEventInfo();
    }
}
