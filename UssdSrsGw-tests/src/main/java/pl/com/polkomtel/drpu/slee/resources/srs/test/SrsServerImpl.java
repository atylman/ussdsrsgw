package pl.com.polkomtel.drpu.slee.resources.srs.test;

import org.apache.commons.lang.StringUtils;
import org.omg.CORBA.IntHolder;
import org.omg.CORBA.ShortHolder;
import org.omg.CORBA.StringHolder;

import pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPOA;
import pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess;
import pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError;

/**
 * CORBA service that emulates SRS server and returns validation result in form
 * of two last digits of a voucher code which was received beforehand.
 * 
 * @author adam.tylman
 */
public class SrsServerImpl extends vasPOA {
	
	private static final int FORCED_VALIDATION_RESULT_LENGTH = 2;
	private static final int FORCED_TIMEOUT_INSTRUCTION = 99;
	private static final int TIMEOUT_PERIOD = 60000;

	public void TopUp(String secretNr, String msisdn, short type, String originator, String channel, String xmlin,
			ShortHolder validationResult, IntHolder credit,	StringHolder appProfile, StringHolder xmlout) throws SystemError, NoAccess {
		
		if(StringUtils.isBlank(secretNr) || secretNr.length() < FORCED_VALIDATION_RESULT_LENGTH)
			validationResult.value = 999;
		else {
			validationResult.value = Short.parseShort(secretNr.substring(secretNr.length()-FORCED_VALIDATION_RESULT_LENGTH));
			
			if(validationResult.value == FORCED_TIMEOUT_INSTRUCTION) {
				try {
					Thread.sleep(TIMEOUT_PERIOD);
				} catch (InterruptedException e) {
					throw new NoAccess("SrsSimulator unintentional malfunction (cannot enforce resp timeout)");
				}
			}
		}
		
		credit.value = 0;
		appProfile.value = "";
		xmlout.value = "";
	}

	public void checkService(ShortHolder available) {
		throw new UnsupportedOperationException("SrsSimulator is designed to support TopUp operation only!!");
	}
}
