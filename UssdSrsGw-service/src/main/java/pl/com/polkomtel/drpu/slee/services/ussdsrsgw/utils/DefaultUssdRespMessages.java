package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils;

/**
 * Defines most commonly used USSD error responses.
 * 
 * @author adam.tylman
 */
public class DefaultUssdRespMessages {
	public static final String GENERAL_ERROR = "Blad uslugi";
	public static final String TOP_UP_ERROR = "Blad doladowania";
	public static final String PARAMETER_MISSING = "Niepoprawna skladnia polecenia!";
}
