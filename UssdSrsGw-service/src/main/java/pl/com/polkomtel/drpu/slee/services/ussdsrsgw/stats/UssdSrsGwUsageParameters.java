package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.stats;

/**
 * SBB Usage Parameters interface used for statistics generation.
 * 
 * @author adam.tylman
 */
public interface UssdSrsGwUsageParameters {
    
	/**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementProcessUnstSsReqReceived(long value);

    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementProcessUnstSsRespSent(long value);
    
    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementUssdErrors(long value);
    
    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementGeneralErrors(long value);
    
    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementSrsReqSent(long value);
    
    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementSrsRespReceived(long value);
    
    /**
     * Increment counter.
     *
     * @param value increase counter by this value
     */
    void incrementSrsErrors(long value);
    
    /**
     * Update sample.
     *
     * @param value update sample by this value in miliseconds
     */
    void sampleSrsQueryTime(long value);
}
