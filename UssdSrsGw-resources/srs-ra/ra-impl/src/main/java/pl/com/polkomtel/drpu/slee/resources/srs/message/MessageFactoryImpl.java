package pl.com.polkomtel.drpu.slee.resources.srs.message;

import org.omg.CORBA.UserException;

/**
 * An implementation of {@link MessageFactory} which actually creates CORBA messages.
 * 
 * @author adam.tylman
 */
public class MessageFactoryImpl implements MessageFactory {

	@Override
	public TopUpRequest createTopUpRequestMessage(final String secretNr, final String msisdn, final short type, final String originator, final String channel, final String xmlin) {
		return new TopUpRequest(secretNr, msisdn, type, originator, channel, xmlin);
	}

	@Override
	public TopUpResponse createTopUpResponseMessage(final short validationResult, final int credit, final String appProfile, final String xmlout) {
		return new TopUpResponse(validationResult, credit, appProfile, xmlout);
	}

	@Override
	public TopUpResponse createTopUpResponseMessage(final UserException userException) {
		return new TopUpResponse(userException);
	}
}
