package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils;

import java.io.UnsupportedEncodingException;

import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.map.MAPMAP_DialoguePDU;
import com.opencloud.slee.resources.cgin.map.MAPMAP_OpenInfo;
import com.opencloud.slee.resources.in.datatypes.cc.AddressString;
import com.opencloud.slee.resources.in.datatypes.sms.GSM7BitAlphabet;
import com.opencloud.slee.resources.in.datatypes.sms.GSM7BitPacking;

/**
 * Used for GSM 7-bit conversion.
 *
 * @author adam.tylman
 */
public class GsmUtils {
    
	public static final byte DEFAULT_DATA_CODING_SCHEME = (byte) 0x0F;
	
    private static final String UNENCODABLE_MESSAGE = "Unencodable message.";

    /**
     * Decodes data encoded in GSM 7-bit alphabet into string.
     * 
     * @param data byte array with data to decode
     * @return decoded data
     */
    public static String decode(final byte[] data) {
        final int unpackedLength = GSM7BitPacking.getUnpackedUSSDSize(data, 0, data.length);
        final byte[] unpackedData = new byte[unpackedLength];
        GSM7BitPacking.unpackUSSD(data, 0, unpackedData, 0, data.length);
        return GSM7BitAlphabet.decode(unpackedData, 0, unpackedLength);
    }

    /**
     * Encodes a given string using the GSM 7-bit alphabet.
     * 
     * @param str string to encode
     * @return encoded string
     * @throws UnsupportedEncodingException if sth with encoding went wrong
     */
    public static byte[] encode(final String str) throws UnsupportedEncodingException {
        String ussdString = str;

        int unpackedLength = GSM7BitAlphabet.countCharacters(ussdString);
        if (unpackedLength <= 0) {
            ussdString = UNENCODABLE_MESSAGE;
            unpackedLength = GSM7BitAlphabet.countCharacters(ussdString);
        }
        
        final byte[] unpackedData = new byte[unpackedLength];
        GSM7BitAlphabet.encode(ussdString, unpackedData, 0);
        final byte[] packedData = new byte[GSM7BitPacking.getPackedUSSDSize(unpackedData, 0, unpackedLength)];
        GSM7BitPacking.packUSSD(unpackedData, 0, packedData, 0, unpackedLength);
        return packedData;
    }
    
    /**
     * Extracts MSISDN from MapDialoguePDU 
     * 
     * @param event initial dialog object
     * @return destination reference number or <code>null</code> if it's not present
     */
    public static AddressString extractMsisdnFromMapDialogPdu(final DialogOpenRequestEvent event) {

        final Object[] userInfo = event.getUserInformation();
        MAPMAP_OpenInfo mapOpen;
		
		for (final Object userInfoItem : userInfo) {
			if (userInfoItem instanceof MAPMAP_DialoguePDU) {
				mapOpen = ((MAPMAP_DialoguePDU) userInfoItem).getMap_open();
				if (mapOpen != null && mapOpen.hasDestinationReference())
					return mapOpen.getDestinationReference();
			}
		}
        
        return null;
    }
}
