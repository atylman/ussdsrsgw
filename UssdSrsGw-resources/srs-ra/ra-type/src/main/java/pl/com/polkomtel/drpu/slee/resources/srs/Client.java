package pl.com.polkomtel.drpu.slee.resources.srs;

import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpRequest;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpResponse;

/**
 * Client interface which enables to initiate requests over CORBA
 * 
 * @author adam.tylman
 */
public interface Client {

	/**
	 * Sends a request over the network to the server instance
	 * @return response message
	 */
	public TopUpResponse performRequest(TopUpRequest message) throws ClientRequestException;

	/**
	 * Used for releasing all allocated resources
	 */
	void destroy();
}