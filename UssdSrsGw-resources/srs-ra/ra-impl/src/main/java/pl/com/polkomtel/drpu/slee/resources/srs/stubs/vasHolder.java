package pl.com.polkomtel.drpu.slee.resources.srs.stubs;

/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public final class vasHolder	implements org.omg.CORBA.portable.Streamable{
	 public vas value;
	public vasHolder()
	{
	}
	public vasHolder (final vas initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return vasHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = vasHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		vasHelper.write (_out,value);
	}
}
