package pl.com.polkomtel.drpu.slee.resources.srs;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import java.util.Arrays;

import javax.slee.facilities.AlarmLevel;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ConfigProperties.Property;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import pl.com.polkomtel.drpu.slee.resources.srs.SrsResourceAdaptor.State;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames={"pl.com.polkomtel.drpu.slee.resources.srs.*", "javax.slee.facilities.*", "org.omg.CORBA.ORB"})
public class SrsResourceAdaptorTest extends BaseResourceAdaptorTest {

	@Test
	public final void verifyValidConfiguration() throws Exception {
		Property[] properties = {
				new Property("remoteCorbaloc", String.class.getName(), VALID_REMOTE_CORBALOC),
				new Property("clientMaxRetries", Integer.class.getName(), 5)
		};
		
		try {
			raSpy.raConfigure(new ConfigProperties(properties));
		} catch(Exception ex) {
			fail("No exception should be thrown for such valid props: " + Arrays.toString(properties));
		}

		verifyPrivate(raSpy).invoke("setState", eq(State.INACTIVE));
	}
	
	@Test
	public final void verifyInvalidMaxRetriesConfiguration() throws Exception {
		Property[] properties = {
				new Property("remoteCorbaloc", String.class.getName(), VALID_REMOTE_CORBALOC),
				new Property("clientMaxRetries", Integer.class.getName(), 50)
		};
		
		assertValidationExceptionThrown(properties, "The clientMaxRetries env entry should be in range of (0,10>");
	}
	
	@Test
	public final void verifyInvalidCorbalocConfiguration() throws Exception {
		Property[] properties = {
				new Property("remoteCorbaloc", String.class.getName(), INVALID_REMOTE_CORBALOC),
				new Property("clientMaxRetries", Integer.class.getName(), 5)
		};
		
		assertValidationExceptionThrown(properties, "The remoteCorbaloc env entry is missing or doesn't match");
	}
	
	@Test
	public final void verifySuccessfulInit() throws Exception {
		Property[] properties = {
				new Property("remoteCorbaloc", String.class.getName(), VALID_REMOTE_CORBALOC),
				new Property("clientMaxRetries", Integer.class.getName(), 5)
		};
		raSpy.raConfigure(new ConfigProperties(properties));
		
		mockCorbaStubs(false);
		raSpy.raActive();

		verifyPrivate(raSpy).invoke("setState", eq(State.ACTIVE));
		assertNoAlarmsRaised();
		
		ClientServerRef clientServerRef = Whitebox.getInternalState((ClientImpl) Whitebox.getInternalState(raSpy, "client"), "requestsPerformer");
		assertTrue("ClientServerRef should be marked as valid for successful Corba init", clientServerRef.isValid());
		assertTrue("ClientServerRef should have its ReinitThread running", clientServerRef.printStatus().contains("reinitTread.running=true"));
	}

	@Test
	public final void verifyFailedInit() throws Exception {
		Property[] properties = {
				new Property("remoteCorbaloc", String.class.getName(), VALID_REMOTE_CORBALOC),
				new Property("clientMaxRetries", Integer.class.getName(), 5)
		};
		raSpy.raConfigure(new ConfigProperties(properties));
		
		mockCorbaStubs(true);
		raSpy.raActive();

		verify(alarmFacilityMock, atLeastOnce()).raiseAlarm(eq("plk.srsra.CorbaConnectionError"), eq("SrsClient"), eq(AlarmLevel.MAJOR), eq("ClientServerRef failed to initialize"));
		verifyPrivate(raSpy).invoke("setState", eq(State.ACTIVE));
		
		ClientServerRef clientServerRef = Whitebox.getInternalState((ClientImpl) Whitebox.getInternalState(raSpy, "client"), "requestsPerformer");
		assertFalse("ClientServerRef should be marked as invalid for unsuccessful Corba init", clientServerRef.isValid());
		assertTrue("ClientServerRef should have its ReinitThread running", clientServerRef.printStatus().contains("reinitTread.running=true"));
		
		try {
			raSpy.getClient().performRequest(null);
			fail("ClientRequestException should be thrown when trying to perform any req with invalid ClientServerRef");
		} catch(ClientRequestException ex) {
			assertThat(ex).isInstanceOf(ClientRequestException.class).hasMessage("No valid client connection");
		}
	}
}
