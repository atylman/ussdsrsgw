package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Common purpose helper methods.
 * 
 * @author adam.tylman
 */
public class GeneralUtils {
	
	public static final String EMPTY_STRING = "";
	public static final int SHORT_MSISDN_LENGTH = 9;
	public static final String DEFAULT_PREFIX = "48";
	
	private static final String USSD_CODE_DELIMITER = "*";
	private static final String USSD_CODE_DELIMITER_REGEX = "\\*";
	private static final String USSD_CODE_DELIMITER_MISTAKEN = "#";
	private static final String USSD_CODE_ENDPOINT = "#";
	private static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSZ";
	
	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	/**
     * Compares a given string against indicated pattern.
     * 
     * @param ussdCodesList string to compare
     * @param pattern a compiled pattern to be used for comparison
     * @return boolean result of comparison
     */
    public static boolean isPatternMatched(final String ussdCodesList, final Pattern pattern) {
		if(StringUtils.isBlank(ussdCodesList) || !pattern.matcher(ussdCodesList).matches())
			return false;
		else
			return true;
	}
    
    /**
     * Parses a typical USSD string and returns an array of parameters.
     * 
     * @param ussdMsg USSD string to parse
     * @return String[] an array containing all parameters that were separated by {@link GeneralUtils#USSD_CODE_DELIMITER} sign 
     */
    private static String[] parseUssdMsg(final String ussdMsg) {
    	if(StringUtils.isBlank(ussdMsg))
    		return null;
    	
    	String msg = ussdMsg;
    	if(msg.startsWith(USSD_CODE_DELIMITER) || msg.startsWith(USSD_CODE_DELIMITER_MISTAKEN))
    		msg = msg.substring(1);
    	if(msg.endsWith(USSD_CODE_ENDPOINT))
    		msg = msg.substring(0, msg.length() - 1);
    	if(msg.contains(USSD_CODE_DELIMITER_MISTAKEN))
    		msg = msg.replace(USSD_CODE_DELIMITER_MISTAKEN, USSD_CODE_DELIMITER);
    	
    	return msg.split(USSD_CODE_DELIMITER_REGEX);
    }
    
    /**
     * Retrieves a single parameter from the entire USSD message.
     * 
     * @param ussdMsg USSD message to filter
     * @param idx index of the parameter to be retrieved
     * @return String extracted parameter
     */
    public static String extractUssdMsgParam(final String ussdMsg, final int idx) {
    	final String[] params = parseUssdMsg(ussdMsg);
    	if(params != null && params.length >= idx + 1)
    		return params[idx];
    	else
    		return EMPTY_STRING;
	}
    
    /**
     * Converts timestamp in miliseconds to date format.
     * 
     * @param timestampMillis timestamp in miliseconds
     * @return String date format
     */
    public static String dateMillisToString(final long timestampMillis) {
		return new SimpleDateFormat(SIMPLE_DATE_FORMAT).format(new Date(timestampMillis));
	}
    
    /**
     * Converts an array of bytes to hex representation.
     * 
     * @param bytes array of bytes
     * @return String hex format
     */
    public static String bytesToHex(final byte[] bytes) {
    	int v;
    	final char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}
}
