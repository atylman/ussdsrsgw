package pl.com.polkomtel.drpu.slee.resources.srs.stubs;


/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public final class vasHelper
{
	private volatile static org.omg.CORBA.TypeCode _type;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			synchronized(vasHelper.class)
			{
				if (_type == null)
				{
					_type = org.omg.CORBA.ORB.init().create_interface_tc("IDL:vas:1.0", "vas");
				}
			}
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas s)
	{
			any.insert_Object(s);
	}
	public static pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas extract(final org.omg.CORBA.Any any)
	{
		return narrow(any.extract_Object()) ;
	}
	public static String id()
	{
		return "IDL:vas:1.0";
	}
	public static vas read(final org.omg.CORBA.portable.InputStream in)
	{
		return narrow(in.read_Object(pl.com.polkomtel.drpu.slee.resources.srs.stubs._vasStub.class));
	}
	public static void write(final org.omg.CORBA.portable.OutputStream _out, final pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas s)
	{
		_out.write_Object(s);
	}
	public static pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
		{
			return null;
		}
		else if (obj instanceof pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas)
		{
			return (pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas)obj;
		}
		else if (obj._is_a("IDL:vas:1.0"))
		{
			pl.com.polkomtel.drpu.slee.resources.srs.stubs._vasStub stub;
			stub = new pl.com.polkomtel.drpu.slee.resources.srs.stubs._vasStub();
			stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
			return stub;
		}
		else
		{
			throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
		}
	}
	public static pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas unchecked_narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
		{
			return null;
		}
		else if (obj instanceof pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas)
		{
			return (pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas)obj;
		}
		else
		{
			pl.com.polkomtel.drpu.slee.resources.srs.stubs._vasStub stub;
			stub = new pl.com.polkomtel.drpu.slee.resources.srs.stubs._vasStub();
			stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
			return stub;
		}
	}
}
