package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage;

/**
 * Generated from IDL exception "SystemError".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public final class SystemErrorHolder
	implements org.omg.CORBA.portable.Streamable
{
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError value;

	public SystemErrorHolder ()
	{
	}
	public SystemErrorHolder(final pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemErrorHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemErrorHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemErrorHelper.write(_out, value);
	}
}
