package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.lang.reflect.Field;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.Tracer;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.profile.ProfileFacility;
import javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory;
import javax.slee.serviceactivity.ServiceActivityFactory;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;

import pl.com.polkomtel.drpu.slee.resources.srs.CommunicationProvider;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGwSbb;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGwCmp.Mode;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.alarm.UssdSrsGwAlarms;

import com.nsn.ploc.feniks.slee.mocks.SbbTest;
import com.nsn.ploc.feniks.slee.nsnbaselib.BaseSbb;
import com.nsn.ploc.feniks.slee.nsnbaselib.alarm.CommonServiceAlarms;
import com.nsn.ploc.feniks.slee.resources.eventreporter.EventReporterProvider;
import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;
import com.opencloud.slee.resources.cgin.Code;
import com.opencloud.slee.resources.cgin.ComponentEvent;
import com.opencloud.slee.resources.cgin.Dialog;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.SccpAddress;
import com.opencloud.slee.resources.cgin.TcapApplicationContext;
import com.opencloud.slee.resources.cgin.TcapOperation;
import com.opencloud.slee.resources.cgin.map.MAPDialog;
import com.opencloud.slee.resources.cgin.map.MAPMAP_DialoguePDU;
import com.opencloud.slee.resources.cgin.map.MAPMAP_OpenInfo;
import com.opencloud.slee.resources.cgin.map.MAPUSSD_Arg;
import com.opencloud.slee.resources.cgin.map.events.MAPProcessUnstructuredSS_RequestRequestEvent;
import com.opencloud.slee.resources.in.datatypes.cc.AddressString;

public abstract class BaseSbbTest extends SbbTest implements UssdSrsGwSbbTestConst {
	
	protected UssdSrsGwSbb sbbSpy;
	protected InitialContext initialCtxMock;
	
	@Mock protected CommunicationProvider corbaProviderMock;
	@Mock protected MAPDialog dialogMock;
	
	@Mock protected AlarmFacility alarmFacilityMock;
	@Mock protected ProfileFacility profileFacilityMock;
	@Mock protected TimerFacility timerFacilityMock;
	@Mock protected ActivityContextNamingFacility acNamingFacilityMock;
	@Mock protected NullActivityContextInterfaceFactory naciFactoryMock;
	@Mock protected NullActivityFactory naFactoryMock;
	@Mock protected ServiceActivityFactory serviceActivityFactoryMock;
	@Mock protected ServiceActivityContextInterfaceFactory serviceActivityContextInterfaceFactoryMock;
	@Mock protected EventReporterProvider reporterMock;
	
	@Rule
    public TestName testName = new TestName();
	
	protected String dumpCurrentMethodName() {
		System.out.println(String.format("\nTest: %s", testName.getMethodName()));
		
		return testName.getMethodName();
    }
	
	protected Object getSbbPrivateField(String fieldName) throws Exception {
		Field field = UssdSrsGwSbb.class.getDeclaredField(fieldName);
		field.setAccessible(true);
		return field.get(null);
	}
	
	@Override
	protected Tracer getMockedTracer() {
		Tracer mockedTracer = mock(Tracer.class);
		when(mockedTracer.getTracerName()).thenReturn("nsn");
		when(mockedTracer.isInfoEnabled()).thenReturn(true);
		when(mockedTracer.isFineEnabled()).thenReturn(true);
		when(mockedTracer.isFinerEnabled()).thenReturn(true);
		when(mockedTracer.isFinestEnabled()).thenReturn(true);
		return mockedTracer;
	}
	
	@Override
	protected void mockReporter(BaseSbb sbb) {
		SessionTraceContext stx = new SessionTraceContext("101-1422628754-58371", UssdSrsGwSbb.TRACER_NAME, false);
		when(reporterMock.generateSessionTraceContext(anyString())).thenReturn(stx);
	}

	protected void mockInitialContext() throws Exception {
		mockInitialContext(VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES[0],
								VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM[0],
								VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM[0]);
	}
	
	protected void mockInitialContext(String codes, String channel, Short type) throws Exception {
		MockitoAnnotations.initMocks(this);
		
		Context mockedCtx = mock(Context.class, Mockito.RETURNS_DEEP_STUBS);
		when(mockedCtx.lookup((String) getSbbPrivateField("SRS_PROVIDER_JNDI_NAME"))).thenReturn(corbaProviderMock);
		when(mockedCtx.lookup((String) getSbbPrivateField("ENV_ENTRY_USSD_ACCEPTABLE_CODES"))).thenReturn(codes);
		when(mockedCtx.lookup((String) getSbbPrivateField("ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM"))).thenReturn(channel);
		when(mockedCtx.lookup((String) getSbbPrivateField("ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM"))).thenReturn(type);

		initialCtxMock = mock(InitialContext.class, Mockito.RETURNS_DEEP_STUBS);
		when(initialCtxMock.lookup("java:comp/env")).thenReturn(mockedCtx);
		mockReporter(null);
		mockGeneralJndi(initialCtxMock);
		
		whenNew(InitialContext.class).withNoArguments().thenReturn(initialCtxMock);
	}
	
	private void mockGeneralJndi(InitialContext mockedIctx) throws NamingException {
		when(mockedIctx.lookup("java:comp/env/slee/facilities/alarm")).thenReturn(alarmFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/profile")).thenReturn(profileFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/timer")).thenReturn(timerFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/activitycontextnaming")).thenReturn(acNamingFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory")).thenReturn(naciFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/nullactivity/factory")).thenReturn(naFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/serviceactivity/factory")).thenReturn(serviceActivityFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/serviceactivity/activitycontextinterfacefactory")).thenReturn(serviceActivityContextInterfaceFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/resources/er/provider")).thenReturn(reporterMock);
	}
		
	protected void assertValidationExceptionThrown(String invalidParam, String exceptionMsg) throws Exception {
		sbbSpy = spy(new UssdSrsGwSbbMock());
		try {
			mockSbbEnvironment(sbbSpy);
			fail("RuntimeException should be thrown due to invalid env entry: " + invalidParam);
		} catch(Exception ex) {
			assertThat(ex).isInstanceOf(RuntimeException.class).hasMessage(exceptionMsg);
		}

		PowerMockito.verifyPrivate(sbbSpy).invoke(
				"raiseAlarm",
				eq(UssdSrsGwAlarms.INVALID_CONFIGURATION),
				eq(CommonServiceAlarms.EMPTY_ALARM_ID), any(Object[].class));
	}
	
	protected void assertAlarmsCleared(String alarmType) throws Exception {
		PowerMockito.verifyPrivate(sbbSpy).invoke("clearAlarms", eq(alarmType));
	}
	
	protected MAPProcessUnstructuredSS_RequestRequestEvent mockProcessUnstrSsEvent(byte[] ussdString) {
		TcapOperation tcapOp = new TcapOperation(null, null, "", "", "",
				null, new Code().setLocal(59),
				TcapOperation.OperationClass.CLASS1, null, false, null, false,
				null, null, null, null, null, null);
		
		MAPUSSD_Arg argument = new MAPUSSD_Arg();
		argument.setUssd_DataCodingScheme(new byte[] {(byte) 0x0F});
		argument.setUssd_String(ussdString);
		
		MAPProcessUnstructuredSS_RequestRequestEvent reqEvent = new MAPProcessUnstructuredSS_RequestRequestEvent(
				dialogMock, tcapOp, argument, 0, false, 0);
		
		return reqEvent;
	}
	
	protected DialogOpenRequestEvent mockDialogOpenEvent(TcapApplicationContext acn, byte[] ussdString) {
		when(dialogMock.getRemoteTransactionID()).thenReturn(new byte[] {(byte) 0x29, (byte) 0x87, (byte) 0x16, (byte) 0x34});
		
		MAPMAP_OpenInfo mapOpen = new MAPMAP_OpenInfo();
		mapOpen.setDestinationReference(new AddressString(AddressString.Nature.INTERNATIONAL, AddressString.NumberingPlan.ISDN, "48600333222"));
		MAPMAP_DialoguePDU userInfoItem = new MAPMAP_DialoguePDU();
		userInfoItem.setMap_open(mapOpen);
		
		DialogOpenRequestEvent event = new DialogOpenRequestEvent(
				(Dialog) dialogMock,
				new SccpAddress(SccpAddress.Type.C7), new SccpAddress(SccpAddress.Type.C7),
				true, 14602, true, 14981, acn,
				new Object[] {userInfoItem},
				new ComponentEvent[] {mockProcessUnstrSsEvent(ussdString)});
		
		return event;
	}
	
	protected InitialEventSelector mockInitialEventSelector(TcapApplicationContext acn, byte[] ussdString) {	
		DialogOpenRequestEvent event = mockDialogOpenEvent(acn, ussdString);
		InitialEventSelector ies = mock(InitialEventSelector.class, Mockito.RETURNS_DEEP_STUBS);
		when(ies.getEvent()).thenReturn(event);
		
		return ies;
	}
	
	protected void testMsgProcessing(MAPProcessUnstructuredSS_RequestRequestEvent event) {
		when(sbbSpy.getOriginMsisdn()).thenReturn("48987654321");
    	sbbSpy.onProcessUnstructuredSSRequest(event, mock(ActivityContextInterface.class));
	}
	
	protected void verifyCorrectMsgProcessing(Mode expectedMode, String expectedRechargedMsisdn, String expectedUssdResp) throws Exception {
		verify(sbbSpy).setRechargeMode(eq(expectedMode));
    	verify(sbbSpy).setUssdCode(eq("123"));
    	verify(sbbSpy).setVoucherId(eq("0123456789012345"));
    	verify(sbbSpy).setRechargedMsisdn(eq(expectedRechargedMsisdn));
    	verifyPrivate(sbbSpy).invoke("sendCorbaRequest", eq("0123456789012345"), eq(expectedRechargedMsisdn), eq("48987654321"));
    	verify(sbbSpy).sendUssdResponse(eq(expectedUssdResp), any(ActivityContextInterface.class));
	}

	
}