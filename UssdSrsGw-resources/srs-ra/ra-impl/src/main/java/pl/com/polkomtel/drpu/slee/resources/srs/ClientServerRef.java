package pl.com.polkomtel.drpu.slee.resources.srs;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.Tracer;

import org.omg.CORBA.ORB;
import org.omg.CORBA.SystemException;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas;
import pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasHelper;

/**
 * Responsible for establishing and maintaining ORB relationship and the actual connection to the SRS server.
 * 
 * @author adam.tylman
 */
class ClientServerRef {
	
	private static final long initFailedWarningInterval = 15000L;

	private final Tracer tracer;
	private final AlarmFacility alarmFacility;
	
	private final String corbaloc;
	private final ReinitThread reinitTread;
	
	private vas server = null;
	private ORB orb = null;
	private boolean valid;
	private String connectionAlarmId = null;
	private long lastInitFailedTimestamp = 0L;

	private class ReinitThread extends Thread {
		private boolean running = true;

		@Override
		public void run() {
			super.run();
			try {
				sleep(5000);
			} catch (final InterruptedException e) {
				tracer.warning("ClientServerRef.ReinitThread:run, thread was interrupted still before looping", e);
			}
			while (isRunning()) {
				tryInit();
				try {
					sleep(500);
				} catch (final InterruptedException e) {
					tracer.warning("ClientServerRef.ReinitThread:run, thread was interrupted", e);
				}
			}
		}

		private synchronized boolean isRunning() {
			return running;
		}

		private synchronized void setNotRunning() {
			this.running = false;
		}
	}

	/**
	 * Default constructor
	 */
	public ClientServerRef(final String corbaloc, final Tracer tracer, final AlarmFacility alarmFacility) {
		super();
		this.tracer = tracer;
		this.alarmFacility = alarmFacility;
		this.corbaloc = corbaloc;
		valid = false;
		tryInit();
		reinitTread = new ReinitThread();
		reinitTread.start();
	}

	private void init(final String corbaloc) throws InvalidName, NotFound, CannotProceed, InvalidName {
		final Properties props = new Properties();
		props.setProperty("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
		props.setProperty("org.omg.CORBA.ORBSingletonClass", "org.jacorb.orb.ORBSingleton");
		props.setProperty("org.omg.PortableInterceptor.ORBInitializerClass.bidir_init", "org.jacorb.orb.giop.BiDirConnectionInitializer");

		//Remark: connect_timeout seems to be eventually set at the value of connect_timeout/10000 minutes 
		props.setProperty("jacorb.connection.client.connect_timeout", "5000");
		props.setProperty("jacorb.connection.client.pending_reply_timeout", "20000");
		
		if(tracer.isFinerEnabled()) {
			try (StringWriter writer = new StringWriter()) {
				props.list(new PrintWriter(writer));
				tracer.finer("ClientServerRef:init, ORB with the following props is going to be initialized:\n" + writer.getBuffer().toString());
			} catch (final IOException ex) {
				tracer.warning("ClientServerRef:init, unable to list ORB initialization pros", ex);
			}
		}
		
		orb = ORB.init((String[]) null, props);
		
		if(tracer.isFinerEnabled())
			tracer.finer("ClientServerRef:init, CORBA object reference is going to be initialized to the following corbaloc: " + corbaloc);
		final org.omg.CORBA.Object obj = orb.string_to_object(corbaloc);
		server = vasHelper.narrow(obj);
	}

	private void tryInit() {
		if (!isValid()) {
			try {
				init(corbaloc);
				if(tracer.isInfoEnabled())
					tracer.info("ClientServerRef:tryInit, client initialized");
				setValid(true);
				if(connectionAlarmId != null) {
					alarmFacility.clearAlarm(connectionAlarmId);
					connectionAlarmId = null;
				}
			} catch (InvalidName | NotFound | CannotProceed | SystemException e) {
				if(System.currentTimeMillis() - lastInitFailedTimestamp >= initFailedWarningInterval) {
					tracer.warning("ClientServerRef:tryInit, client failed to initialize, exception thrown: ", e);
					lastInitFailedTimestamp = System.currentTimeMillis();
				}
				
				setValid(false);
				if(connectionAlarmId == null)
					connectionAlarmId = alarmFacility.raiseAlarm(tracer.getTracerName()+".CorbaConnectionError", "SrsClient", AlarmLevel.MAJOR, "ClientServerRef failed to initialize");
				
				try {
					clear();
				} catch (final Exception ex) {
					tracer.severe("ClientServerRef:tryInit, client not cleared", ex);
				}
			}
		}
	}

	synchronized void markInvalid(final SystemException ex) {
		tracer.severe(String.format("ClientServerRef:markInvalid, corbaloc [%s]", corbaloc));
		if(connectionAlarmId == null)
			connectionAlarmId = alarmFacility.raiseAlarm(tracer.getTracerName()+".CorbaConnectionError", "SrsClient", AlarmLevel.MAJOR, "ClientServerRef marked as invalid", ex);
		try {
			clear();
		} catch (final Exception e) {
			tracer.severe("ClientServerRef:markInvalid, client not cleared", e);
		}
		if (tracer.isFinestEnabled())
			tracer.finest("ClientServerRef:markInvalid, marking invalid");

		setValid(false);
	}

	synchronized boolean isValid() {
		return valid;
	}

	synchronized private void setValid(final boolean valid) {
		this.valid = valid;
	}

	void clear() {
		if (tracer.isFinestEnabled())
			tracer.finest("ClientServerRef:clear, clearing performer");

		if (server != null)
			server._release();

		if (orb != null) {
			orb.destroy();
			orb = null;
		}
	}

	vas getServer() {
		return server;
	}

	void stopReinit() {
		reinitTread.setNotRunning();
	}
	
	/**
	 * Shorter version of {@link java.lang.Object#toString()} method
	 */
	synchronized String printStatus() {
		final StringBuilder builder = new StringBuilder("ClientServerRef [valid=");
		builder.append(valid);
		builder.append(", corbaloc=");
		builder.append(corbaloc);
		builder.append(", reinitTread.running=");
		builder.append(reinitTread.isRunning());
		builder.append(']');
		return builder.toString();
	}
}
