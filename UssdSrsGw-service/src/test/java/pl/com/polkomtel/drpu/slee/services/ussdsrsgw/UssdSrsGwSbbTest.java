package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import java.util.Arrays;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.InitialEventSelector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import pl.com.polkomtel.drpu.slee.resources.srs.message.ValidationResult;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGwCmp.Mode;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.alarm.UssdSrsGwAlarms;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.DefaultUssdRespMessages;

import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.map.events.MAPProcessUnstructuredSS_RequestRequestEvent;
import com.opencloud.slee.resources.cgin.map.metadata.MAPApplicationContexts;
import com.opencloud.slee.resources.in.datatypes.cc.AddressString;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames={"pl.com.polkomtel.drpu.slee.services.ussdsrsgw.*", "com.nsn.ploc.feniks.slee.nsnbaselib.BaseSbb", "javax.slee.facilities.*"})
public class UssdSrsGwSbbTest extends BaseSbbTest {
	
	public final static String[] skipSetUpForMethods = {
			"verifyInvalidSbbEnvEntryUssdCodes",
			"verifyInvalidSbbEnvEntryChannelParams",
			"verifyInvalidSbbEnvEntryTypeParams",
			"verifyValidSbbEnvEntries" };

	@Before
    public final void setUp() throws CreateException, Exception {
		if(Arrays.asList(skipSetUpForMethods).contains(dumpCurrentMethodName()))
			return;
		sbbSpy = spy(new UssdSrsGwSbbMock());
		mockInitialContext();
        mockSbbEnvironment(sbbSpy);
    }

	@Test
    public final void verifyInvalidSbbEnvEntryUssdCodes() throws Exception {
		String exceptionMsg = String
				.format("Invalid configuration, failed to validate environment variables: The [%s] is missing or doesn't match [%s] pattern",
						(String) getSbbPrivateField("ENV_ENTRY_USSD_ACCEPTABLE_CODES"),
						(String) getSbbPrivateField("ENV_ENTRY_USSD_ACCEPTABLE_CODES_REGEX"));

		for(String invalidValue : INVALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES) {
			mockInitialContext(invalidValue, VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM[0], VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM[0]);
			assertValidationExceptionThrown(invalidValue, exceptionMsg);
		}
	}
	
	@Test
    public final void verifyInvalidSbbEnvEntryChannelParams() throws Exception {
		String exceptionMsg = 
				String.format("Invalid configuration, failed to validate environment variables: The [%s] cannot be empty",
								(String) getSbbPrivateField("ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM"));
		
		for(String invalidValue : INVALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAMS) {
			mockInitialContext(VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES[0], invalidValue, VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM[0]);
			assertValidationExceptionThrown(invalidValue, exceptionMsg);
		}
	}
	
	@Test
    public final void verifyInvalidSbbEnvEntryTypeParams() throws Exception {
		String exceptionMsg = 
				String.format("Invalid configuration, failed to validate environment variables: The [%s] cannot be empty",
								(String) getSbbPrivateField("ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM"));
		
		for(Short invalidValue : INVALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAMS) {
			mockInitialContext(VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES[0], VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM[0], invalidValue);
			assertValidationExceptionThrown(String.valueOf(invalidValue), exceptionMsg);
		}
	}
	
	@Test
    public final void verifyValidSbbEnvEntries() throws Exception {
		for(String validValue : VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES) {
			sbbSpy = spy(new UssdSrsGwSbbMock());
			mockInitialContext(validValue, VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM[0], VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM[0]);
			mockSbbEnvironment(sbbSpy);
			assertAlarmsCleared(UssdSrsGwAlarms.INVALID_CONFIGURATION.getAlarmType());
		}
		
		for(String validValue : VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM) {
			sbbSpy = spy(new UssdSrsGwSbbMock());
			mockInitialContext(VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES[0], validValue, VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM[0]);
			mockSbbEnvironment(sbbSpy);
			assertAlarmsCleared(UssdSrsGwAlarms.INVALID_CONFIGURATION.getAlarmType());
		}
		
		for(Short validValue : VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM) {
			sbbSpy = spy(new UssdSrsGwSbbMock());
			mockInitialContext(VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES[0], VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM[0], validValue);
			mockSbbEnvironment(sbbSpy);
			assertAlarmsCleared(UssdSrsGwAlarms.INVALID_CONFIGURATION.getAlarmType());
		}
	}

	@Test
    public final void verifyInitialEventRejectedDueToWrongAcn() throws Exception {
		InitialEventSelector ies = mockInitialEventSelector(
				MAPApplicationContexts.shortMsgMO_RelayContext_v1_ac,
				VALID_USSD_STRING_STANDARD_MODE);
		sbbSpy.initialEventSelect(ies);
		verify(ies).setInitialEvent(eq(false));
	}
	
	@Test
    public final void verifyInitialEventRejectedDueToWrongCode() throws Exception {
		InitialEventSelector ies = mockInitialEventSelector(
				MAPApplicationContexts.networkUnstructuredSsContext_v2_ac,
				INVALID_USSD_STRING_WRONG_CODE);
		sbbSpy.initialEventSelect(ies);
		verifyPrivate(sbbSpy).invoke("isUssdCodeMatched", any(DialogOpenRequestEvent.class));
		verify(ies).setInitialEvent(eq(false));
	}
	
	@Test
    public final void verifyInitialEventAccepted() throws Exception {
		InitialEventSelector ies = mockInitialEventSelector(
				MAPApplicationContexts.networkUnstructuredSsContext_v2_ac,
				VALID_USSD_STRING_STANDARD_MODE);
		sbbSpy.initialEventSelect(ies);
		verifyPrivate(sbbSpy).invoke("isUssdCodeMatched", any(DialogOpenRequestEvent.class));
		verify(ies).setInitialEvent(eq(true));
	}
	
	@Test
    public final void verifyInitialEventAcceptedWithUssdMissingParams() throws Exception {
		InitialEventSelector ies;
		for(int idx = 0; idx < INVALID_USSD_STRING_MISSING_PARAMS.length; idx++) {
			ies = mockInitialEventSelector(
					MAPApplicationContexts.networkUnstructuredSsContext_v2_ac,
					INVALID_USSD_STRING_MISSING_PARAMS[idx]);
			sbbSpy.initialEventSelect(ies);
			verifyPrivate(sbbSpy, times(idx+1)).invoke("isUssdCodeMatched", any(DialogOpenRequestEvent.class));
			verify(ies).setInitialEvent(eq(true));
		}
	}

	@Test
    public final void verifyCorrectMsgProcessingInStandardMode() throws Exception {
		MAPProcessUnstructuredSS_RequestRequestEvent event = mockProcessUnstrSsEvent(VALID_USSD_STRING_STANDARD_MODE);
		testMsgProcessing(event);
    	verifyCorrectMsgProcessing(Mode.STANDARD, "48987654321", ValidationResult.VALIDATION_SUCCESSFUL.getDescription());
    }
	
	@Test
    public final void verifyCorrectMsgProcessingInOnBehalfMode() throws Exception {
		MAPProcessUnstructuredSS_RequestRequestEvent event = mockProcessUnstrSsEvent(VALID_USSD_STRING_ON_BEHALF_MODE);
		testMsgProcessing(event);
    	verifyCorrectMsgProcessing(Mode.ON_BEHALF, "48123456789", ValidationResult.VALIDATION_SUCCESSFUL.getDescription());
    }
	
	@Test
    public final void verifyCorrectMsgProcessingInUnspecifiedMode() throws Exception {
		MAPProcessUnstructuredSS_RequestRequestEvent event;
		for(int idx = 0; idx < INVALID_USSD_STRING_MISSING_PARAMS.length; idx++) {
			event = mockProcessUnstrSsEvent(INVALID_USSD_STRING_MISSING_PARAMS[idx]);
			testMsgProcessing(event);
	    	verifyPrivate(sbbSpy, never()).invoke("sendCorbaRequest", anyString(), anyString(), anyString());
	    	verify(sbbSpy, times(idx+1)).sendUssdResponse(eq(DefaultUssdRespMessages.PARAMETER_MISSING), any(ActivityContextInterface.class));
		}
	}

	@Test
	public final void verifyNumberNormalizerChildRelationError() throws Exception {
		doThrow(new CreateException("NumberNormalizerSbb create method returned null result")).when(sbbSpy).getNumberNormalizerSbb();
		doNothing().when(sbbSpy, "initializeTracing", any(SessionTraceContext.class));
		
		DialogOpenRequestEvent event = mockDialogOpenEvent(
				MAPApplicationContexts.networkUnstructuredSsContext_v2_ac,
				VALID_USSD_STRING_STANDARD_MODE);
		
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		verify(dialogMock).acceptDialog();
	}
	
	@Test
	public final void verifyDbQueryChildRelationError() throws Exception {
		doReturn(false).when(sbbSpy, "normalizeCallingPartyNumber", any(AddressString.class));
		doThrow(new CreateException("DbQuerySbb create method returned null result")).when(sbbSpy).getDbQuerySbb();
		doNothing().when(sbbSpy, "initializeTracing", any(SessionTraceContext.class));

		DialogOpenRequestEvent event = mockDialogOpenEvent(
				MAPApplicationContexts.networkUnstructuredSsContext_v2_ac,
				VALID_USSD_STRING_STANDARD_MODE);
		
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		verify(dialogMock).acceptDialog();
	}
}