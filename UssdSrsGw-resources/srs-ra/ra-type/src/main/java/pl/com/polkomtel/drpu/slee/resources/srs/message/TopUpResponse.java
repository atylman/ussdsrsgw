package pl.com.polkomtel.drpu.slee.resources.srs.message;

import java.io.Serializable;

import org.omg.CORBA.UserException;

/**
 * Represents CORBA TopUp response.
 * 
 * @author adam.tylman
 */
public class TopUpResponse implements Serializable {
	private static final long serialVersionUID = 1084084321495664266L;

	private ValidationResult validationResult = ValidationResult.UNKNOWN_VALIDATION_RESULT_CODE;
	private int credit = 0;
	private String appProfile = null;
	private String xmlout = null;
	private UserException userException = null;

	/**
	 * Primary constructor.
	 * 
	 * @param validationResult validation result code
	 * @param credit top-up amount
	 * @param appProfile application profile
	 * @param xmlout optional XML payload for customer specific extensions
	 */
	public TopUpResponse(final short validationResult, final int credit, final String appProfile, final String xmlout) {
		super();
		this.validationResult = ValidationResult.fromInt(validationResult);
		this.credit = credit;
		this.appProfile = appProfile;
		this.xmlout = xmlout;
	}
	
	/**
	 * Constructor used for exception handling only.
	 * 
	 * @param userException exception thrown upon invocation attempt
	 */
	public TopUpResponse(final UserException userException) {
		super();
		this.userException = userException;
	}

	/**
	 * @return the validationResult
	 */
	public ValidationResult getValidationResult() {
		return validationResult;
	}

	/**
	 * @param validationResult the validationResult to set
	 */
	public void setValidationResult(final ValidationResult validationResult) {
		this.validationResult = validationResult;
	}

	/**
	 * @return the credit
	 */
	public int getCredit() {
		return credit;
	}

	/**
	 * @param credit the credit to set
	 */
	public void setCredit(final int credit) {
		this.credit = credit;
	}

	/**
	 * @return the appProfile
	 */
	public String getAppProfile() {
		return appProfile;
	}

	/**
	 * @param appProfile the appProfile to set
	 */
	public void setAppProfile(final String appProfile) {
		this.appProfile = appProfile;
	}

	/**
	 * @return the xmlout
	 */
	public String getXmlout() {
		return xmlout;
	}

	/**
	 * @param xmlout the xmlout to set
	 */
	public void setXmlout(final String xmlout) {
		this.xmlout = xmlout;
	}

	/**
	 * @return the userException
	 */
	public UserException getUserException() {
		return userException;
	}

	/**
	 * @param userException the userException to set
	 */
	public void setUserException(final UserException userException) {
		this.userException = userException;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder("TopUpResponse [");
		if (validationResult != null) {
			builder.append("validationResult=");
			builder.append(validationResult);
			builder.append(", ");
		}
		builder.append("credit=");
		builder.append(credit);
		builder.append(", ");
		if (appProfile != null) {
			builder.append("appProfile=");
			builder.append(appProfile);
			builder.append(", ");
		}
		if (xmlout != null) {
			builder.append("xmlout=");
			builder.append(xmlout);
			builder.append(", ");
		}
		if (userException != null) {
			builder.append("userException=");
			builder.append(userException);
		}
		builder.append(']');
		return builder.toString();
	}
}
