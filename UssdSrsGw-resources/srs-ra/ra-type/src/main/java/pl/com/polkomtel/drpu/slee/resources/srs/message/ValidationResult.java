package pl.com.polkomtel.drpu.slee.resources.srs.message;

/**
 * This enum represents all possible result codes returned by SRS together with a short description that can be
 * forwarded to the user. 
 * 
 * @author adam.tylman
 */
public enum ValidationResult {

	VALIDATION_SUCCESSFUL(0, "Konto zasilone!"),

	UNKNOWN_VOUCHER_NUMBER(1, "Niepoprawny telekod!"),

	SERVICE_PROVIDER_MATCHING_FAILED(2, "Niepoprawny telekod!"),

	INVALID_APPLICATION(3, "Niepoprawny telekod!"),

	VOUCHER_USED_UP(4, "Niepoprawny telekod!"),

	VOUCHER_EXPIRED(5, "Niepoprawny telekod!"),

	ALL_OTHER_VALIDATION_ERRORS(6, "Niepoprawny telekod!"),

	FRAUD_DETECTED(7, "Niepoprawny telekod!"),
	
	VOUCHER_NOT_VALID_YET(11, "Niepoprawny telekod!"),
	
	VOUCHER_BLOCKED(12, "Niepoprawny telekod!"),
	
	VOUCHER_IN_PRODUCTION(13, "Niepoprawny telekod!"),
	
	SYSTEM_DOWNTIME(15, "Przepraszamy, usluga niedostepna. Prosimy sprobowac za kilka minut."),
	
	UNKNOWN_VALIDATION_RESULT_CODE(1000, "Nierozpoznany kod bledu doladowania");

	private final int resultCode;
	private final String description;

	/**
	 * Getter to the numeric value of result code returned by SRS.
	 * @return result code
	 */
	public int getResultCode() {
		return this.resultCode;
	}
	
	/**
	 * Getter to the short description of result code returned by SRS.
	 * @return text description
	 */
	public String getDescription() {
		return this.description;
	}
	
	private ValidationResult(final int resultCode, final String description) {
		this.resultCode = resultCode;
		this.description = description;
	}
	
	/**
	 * Initiates a new enum instance from a numeric value.
	 * @param code numeric value of result code
	 * @return new enum representing such a result code
	 */
	public static ValidationResult fromInt(final int code) {
		for(final ValidationResult validationResult : ValidationResult.values()) {
	        if (validationResult.resultCode == code) {
	            return validationResult;
	        }
	    }
		
		return UNKNOWN_VALIDATION_RESULT_CODE;
	}
}