#!/bin/bash

. ./tests.properties

start_srs_server() {
	echo "Starting SrsSimulator on $SRS_SIMULATOR_IP:$SRS_SIMULATOR_PORT" 2>&1
	nohup java -jar SrsSimulator.jar -host $SRS_SIMULATOR_IP -port $SRS_SIMULATOR_PORT > srs.log 2>&1 &
	echo $! > srs.pid
}

stop_srs_server() {
	if [ -f "srs.pid" ]; then
		echo "Killing SrsSimulator" 2>&1
		kill -9 `cat srs.pid` 2>&1
		wait $! 2>/dev/null
		rm -f srs.pid
	else
		echo "SrsSimulator is not running. No PID file is found!!" 2>&1
	fi
}

get_service_status() {
	if [[ "`$RHINO_CLIENT_CMD getservicestate name=UssdSrsGwService,vendor=Polkomtel,version=$USSDSRSGW_SERVICE_VERSION`" =~ "Active" ]]; then
	   return 1
	else
	    return 0
	fi
}

activate_service() {
	get_service_status
	if [[ $? == 0 ]]; then
		$RHINO_CLIENT_CMD activateservice name=UssdSrsGwService,vendor=Polkomtel,version=$USSDSRSGW_SERVICE_VERSION 2>&1
	fi
}

deactivate_service() {
	get_service_status
	if [[ $? == 1 ]]; then
		$RHINO_CLIENT_CMD deactivateservice name=UssdSrsGwService,vendor=Polkomtel,version=$USSDSRSGW_SERVICE_VERSION 2>&1
	fi
}

get_ra_status() {
	if [[ "`$RHINO_CLIENT_CMD getraentitystate srs-corba-client`" =~ "Active" ]]; then
	   return 1
	else
	    return 0
	fi
}

activate_ra() {
	get_ra_status
	if [[ $? == 0 ]]; then
		$RHINO_CLIENT_CMD activateraentity srs-corba-client 2>&1
	fi
}

deactivate_ra() {
	get_ra_status
	if [[ $? == 1 ]]; then
		$RHINO_CLIENT_CMD deactivateraentity srs-corba-client 2>&1
	fi
}

update_ra() {
	$RHINO_CLIENT_CMD updateraentityconfigproperties srs-corba-client remoteCorbaloc=$1 2>&1
}

config_ra() {
	deactivate_ra
	sleep 1
	update_ra $1
	activate_ra
	sleep 1
}

add_appender_ra() {
	$RHINO_CLIENT_CMD addappenderref trace.srs_corba_client.plk.srsra UssdSrsGwLog
}

remove_appender_ra() {
	$RHINO_CLIENT_CMD removeappenderref trace.srs_corba_client.plk.srsra UssdSrsGwLog
}

clear_alarms() {
	$RHINO_CLIENT_CMD clearalarms resourceadaptorentity srs-corba-client
	$RHINO_CLIENT_CMD clearalarms sbb service=ServiceID[name=UssdSrsGwService,vendor=Polkomtel,version=1.0],sbb=SbbID[name=UssdSrsGwSbb,vendor=Polkomtel,version=1.0]
}

initial_cleanup() {
	stop_srs_server
	rm -f *.log *.pid
	$RHINO_CLIENT_CMD rolloverlogfiles
	add_appender_ra
	sleep 1
}

final_cleanup() {
	deactivate_service
	deactivate_ra
	stop_srs_server
	remove_appender_ra
	clear_alarms
}

#####################################################################
#######################    TESTS EXECUTION    #######################
#####################################################################

echo -e "\n-------------Default OC simulator based TCs--------------"
initial_cleanup

start_srs_server
config_ra $SRS_SIMULATOR_CORBALOC
activate_service
$OC_SIMULATOR_CMD

res_7000=`grep "|7000|" $USSDSRSGW_EVENT_LOG | wc -l | awk {'print $1'}`
res_7002=`grep "|7002|.*|SRS_RESP_CODE:0|$" $USSDSRSGW_EVENT_LOG | wc -l | awk {'print $1'}`
res_7003=`grep "|7003|" $USSDSRSGW_EVENT_LOG | wc -l | awk {'print $1'}`
res_7005=`grep "|7005|.*|ADDITIONAL_INFO:.*timeout.*|$" $USSDSRSGW_EVENT_LOG | wc -l | awk {'print $1'}`
res_7009=`grep "|7009|.*|ADDITIONAL_INFO:USSD msg processing error|$" $USSDSRSGW_EVENT_LOG | wc -l | awk {'print $1'}`

echo -e "\n---------SRS RA set-up with lack of connectivity---------"
$RHINO_CLIENT_CMD rolloverlogfiles
config_ra $SRS_INVALID_CORBALOC

srs_init_failed=`grep "ClientServerRef:tryInit, client failed to initialize" $USSDSRSGW_SERVICE_LOG | wc -l | awk {'print $1'}`
srs_retries_exceeded=`grep "Retries exceeded, couldn't reconnect" $USSDSRSGW_SERVICE_LOG | wc -l | awk {'print $1'}`
srs_activation_successful=`grep "SrsResourceAdaptor transitioned to ACTIVE state" $USSDSRSGW_SERVICE_LOG | wc -l | awk {'print $1'}`
srs_exceptions=`grep "Exception" $USSDSRSGW_SERVICE_LOG | wc -l | awk {'print $1'}`
srs_alarm_raised=`$RHINO_CLIENT_CMD listactivealarms | grep ".*ClientServerRef failed to initialize.*" | wc -l | awk {'print $1'}`

final_cleanup

echo -e "\n-------------------------Summary-------------------------\n
Tests execution has finished with the following results:
 
 service_started = $res_7000 (expected=7)
 service_successfully_ended = $res_7003 (expected=3)
 successful_topups = $res_7002 (expected=2)
 srs_communication_failures = $res_7005 (expected=1)
 msg_processing_errors = $res_7009 (expected=3)

 srs_init_failed = $srs_init_failed (expected=1)
 srs_retries_exceeded = $srs_retries_exceeded (expected=1)
 srs_activation_successful = $srs_activation_successful (expected=1)
 srs_alarm_raised = $srs_alarm_raised (expected=1)
 srs_exceptions = $srs_exceptions (expected=0)\n"