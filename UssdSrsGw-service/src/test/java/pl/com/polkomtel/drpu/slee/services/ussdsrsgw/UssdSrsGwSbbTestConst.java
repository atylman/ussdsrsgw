package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;


public interface UssdSrsGwSbbTestConst {
	String[] VALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES = {"123", "123,124,125"};
	String[] VALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAM = {"VUSSD", "USSD123", "USSD_123"};
	Short[] VALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAM = {0, 1000, -255};
	
	String[] INVALID_ENV_ENTRY_USSD_ACCEPTABLE_CODES = {"0123", "123, 124, 567", "123,0325", "123,124,", "ABCD", "123456"};
	String[] INVALID_ENV_ENTRY_CORBA_TOPUP_REQ_CHANNEL_PARAMS = {"", null};
	Short[] INVALID_ENV_ENTRY_CORBA_TOP_UP_REQ_TYPE_PARAMS = {null};
	
	//*123*0123456789012345#
	byte[] VALID_USSD_STRING_STANDARD_MODE = {(byte) 0xAA, (byte) 0x98, (byte) 0x6C, (byte) 0xA6, (byte) 0x82, (byte) 0xC5, (byte) 0x64, (byte) 0x33, (byte) 0x5A, (byte) 0xCD, (byte) 0x76, (byte) 0xC3, (byte) 0xE5, (byte) 0x60, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0x1B, (byte) 0x01};
	//*123*48123456789*0123456789012345#
	byte[] VALID_USSD_STRING_ON_BEHALF_MODE = {(byte) 0xAA, (byte) 0x98, (byte) 0x6C, (byte) 0xA6, (byte) 0x82, (byte) 0xC5, (byte) 0x64, (byte) 0x33, (byte) 0x5A, (byte) 0xCD, (byte) 0x76, (byte) 0xC3, (byte) 0xE5, (byte) 0x60, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0x53, (byte) 0xD1, (byte) 0x70, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0xB3, (byte) 0xDD, (byte) 0x70, (byte) 0xB9, (byte) 0x11};
	//*120*48123456789*0123456789012345#
	byte[] INVALID_USSD_STRING_WRONG_CODE = {(byte) 0xAA, (byte) 0x98, (byte) 0x0C, (byte) 0xA6, (byte) 0x82, (byte) 0xC5, (byte) 0x64, (byte) 0x33, (byte) 0x5A, (byte) 0xCD, (byte) 0x76, (byte) 0xC3, (byte) 0xE5, (byte) 0x60, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0x53, (byte) 0xD1, (byte) 0x70, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0xB3, (byte) 0xDD, (byte) 0x70, (byte) 0xB9, (byte) 0x11};
	//*123#, #123#, #123#0123456789012345#
	byte[][] INVALID_USSD_STRING_MISSING_PARAMS = {
			{ (byte) 0xAA, (byte) 0x98, (byte) 0x6C, (byte) 0x36, (byte) 0x02 },
			{ (byte) 0xA3, (byte) 0x98, (byte) 0x6C, (byte) 0x36, (byte) 0x02 },
			{ (byte) 0xA3, (byte) 0x98, (byte) 0x6C, (byte) 0x36, (byte) 0x82, (byte) 0xC5, (byte) 0x64, (byte) 0x33, (byte) 0x5A, (byte) 0xCD, (byte) 0x76, (byte) 0xC3, (byte) 0xE5, (byte) 0x60, (byte) 0x31, (byte) 0xD9, (byte) 0x8C, (byte) 0x56, (byte) 0x1B, (byte) 0x01 } };
}