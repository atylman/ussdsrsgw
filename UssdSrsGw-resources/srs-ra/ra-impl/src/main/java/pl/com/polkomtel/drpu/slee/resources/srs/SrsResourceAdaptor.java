package pl.com.polkomtel.drpu.slee.resources.srs;


import java.util.regex.Pattern;

import javax.slee.Address;
import javax.slee.InvalidArgumentException;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.Tracer;
import javax.slee.resource.ActivityHandle;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.FailureReason;
import javax.slee.resource.FireableEventType;
import javax.slee.resource.InvalidConfigurationException;
import javax.slee.resource.Marshaler;
import javax.slee.resource.ReceivableService;
import javax.slee.resource.ResourceAdaptor;
import javax.slee.resource.ResourceAdaptorContext;

import pl.com.polkomtel.drpu.slee.resources.srs.message.MessageFactory;
import pl.com.polkomtel.drpu.slee.resources.srs.message.MessageFactoryImpl;

import com.opencloud.rhino.resourceadaptor.RhinoExtensionProperties;

/**
 * The actual SRS RA object that implements all JSLEE RA lifecycle methods and CORBA enforced functionality. 
 * 
 * @author adam.tylman
 */
public class SrsResourceAdaptor implements ResourceAdaptor, CommunicationProvider {
	
	private static final String TRACER_NAME = "plk.srsra";
	
	private static final String CORBALOC_VALID_REGEX = "^corbaloc:(iiop)?:([0-9]\\.[0-9]@)?[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}:[0-9]*\\/.*";
	private static final Pattern CORBALOC_VALID_PATTERN = Pattern.compile(CORBALOC_VALID_REGEX);

	private Tracer tracer = null;
	private AlarmFacility alarmFacility = null;
	
	private String remoteCorbaloc = null;
	private int clientMaxRetries = 1;
	
	private int nodeID = -1;
	private State state = State.UNCONFIGURED;
	
	private Client client = null;	
	private final MessageFactory messageFactory = new MessageFactoryImpl();
	
	
	enum State {UNCONFIGURED, INACTIVE, ACTIVE};
	
	private boolean initClient() {
		final ClassLoader origContextClassLoader = Thread.currentThread().getContextClassLoader();		
		try {
			Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
			client = new ClientImpl(remoteCorbaloc, tracer, alarmFacility, messageFactory, clientMaxRetries);
		} catch (final Exception e) {
			tracer.severe("initClient: unable to activate", e);
			return false;
		} finally {
			Thread.currentThread().setContextClassLoader(origContextClassLoader);
		}
		return true;
	}
	
	private synchronized State getState() {
		return state;
	}
	
	private synchronized void setState(final State newState) {
		this.state = newState;
	}
	
	private void readParameters(final ConfigProperties properties) throws InvalidArgumentException {
		if(tracer.isFinerEnabled())
			tracer.finer(String.format("SrsResourceAdaptor starts reading config params: %s", properties));

		remoteCorbaloc = (String) properties.getProperty("remoteCorbaloc").getValue();
		clientMaxRetries = ((Integer) properties.getProperty("clientMaxRetries").getValue()).intValue();
		
		if(remoteCorbaloc==null || remoteCorbaloc.trim().isEmpty() || !CORBALOC_VALID_PATTERN.matcher(remoteCorbaloc).matches())
			throw new InvalidArgumentException(
					String.format("The remoteCorbaloc env entry is missing or doesn't match [%s] pattern", CORBALOC_VALID_PATTERN));
		
		if(clientMaxRetries <= 0 || clientMaxRetries > 10)
			throw new InvalidArgumentException(
					String.format("The clientMaxRetries env entry should be in range of (0,10> while now it's equal to [%d]", clientMaxRetries));
	}

	/***********************************************************************
	 *** CommunicationProvider interface enforced methods implementation ***
	 ***********************************************************************/
	
	@Override
	public MessageFactory getMessageFactory() {
		return messageFactory;
	}

	@Override
	public Client getClient() {
		return client;
	}

	/***********************************************************************
	 ****** ResourceAdaptor interface enforced methods implementation ******
	 ***********************************************************************/
	
	@Override
	public void setResourceAdaptorContext(final ResourceAdaptorContext context) {
		this.tracer = context.getTracer(TRACER_NAME);
		this.alarmFacility = context.getAlarmFacility();
	}

	@Override
	public void unsetResourceAdaptorContext() {
		this.tracer = null;
	}

	@Override
	public void raConfigure(final ConfigProperties properties) {
		final ConfigProperties.Property nodeIdProp = properties.getProperty(RhinoExtensionProperties.NODE_ID);
		nodeID = nodeIdProp != null ? (Integer) nodeIdProp.getValue() : 0;

		if(tracer.isFineEnabled())
			tracer.fine(String.format("SrsResourceAdaptor is being configured on node [%d]", nodeID));

		try {
			readParameters(properties);
		} catch (final InvalidArgumentException iae) {
			throw new RuntimeException("Environment entries validation error: "+iae.getMessage(), iae);
		}
		setState(State.INACTIVE);
	}

	@Override
	public void raUnconfigure() {
		if(tracer.isInfoEnabled())
			tracer.info("SrsResourceAdaptor transitioned to UNCONFIGURED state");
		setState(State.UNCONFIGURED);
	}

	@Override
	public void raActive() {
		if (getState() != State.INACTIVE) {
			tracer.warning(String.format("SrsResourceAdaptor cannot be activated while being in state [%s]", getState().name()));
			return;
		}

		final boolean initClient = initClient();
		if(!initClient)
			tracer.severe("SrsResourceAdaptor activation failed");
		else {
			setState(State.ACTIVE);
			if(tracer.isInfoEnabled())
				tracer.info("SrsResourceAdaptor transitioned to ACTIVE state");
		}	
	}

	@Override
	public void raStopping() {
		if(tracer.isFineEnabled())
			tracer.fine("SrsResourceAdaptor stopping...");
		
		if (client != null) {
			try {
				client.destroy();
				client = null;
			} catch (final Exception e) {
				tracer.warning("SrsResourceAdaptor encountered difficulties while releasing ORB client", e);
			}
		}
	}

	@Override
	public void raInactive() {
		if(tracer.isFineEnabled())
			tracer.fine("raInactive");
		
		if (getState() != State.ACTIVE)
			return;
		
		setState(State.INACTIVE);
		if(tracer.isInfoEnabled())
			tracer.info("SrsResourceAdaptor transitioned to INACTIVE state");		
	}

	@Override
	public void raVerifyConfiguration(final ConfigProperties properties) throws InvalidConfigurationException {
		final String tempRemoteCorbaloc = ((String) properties.getProperty("remoteCorbaloc").getValue());
		if (tempRemoteCorbaloc.isEmpty()) {
			tracer.warning("remoteCorbaloc parameter raVerifyConfiguration failed");
			throw new InvalidConfigurationException("remoteCorbaloc property not properly specified");
		}
		
		final int tempClientMaxRetries = ((Integer) properties.getProperty("clientMaxRetries").getValue()).intValue();
		if (tempClientMaxRetries <= 0) {
			tracer.warning("clientMaxRetries parameter raVerifyConfiguration failed");
			throw new InvalidConfigurationException("clientMaxRetries must be a positive number greater than 0");
		}
	}

	@Override
	public void raConfigurationUpdate(final ConfigProperties properties) {
		try {
			readParameters(properties);
		} catch (final InvalidArgumentException iae) {
			throw new RuntimeException("Environment entries validation error", iae);
		}
	}

	@Override
	public Object getResourceAdaptorInterface(final String className) {
		return this;
	}

	@Override
	public Marshaler getMarshaler() {
		return null;
	}

	@Override
	public void serviceActive(final ReceivableService serviceInfo) {
		if(tracer.isFinestEnabled())
			tracer.finest("Nothing to do for SrsResourceAdaptor");
	}

	@Override
	public void serviceStopping(final ReceivableService serviceInfo) {
		if(tracer.isFinestEnabled())
			tracer.finest("Nothing to do for SrsResourceAdaptor");
	}

	@Override
	public void serviceInactive(final ReceivableService serviceInfo) {
		if(tracer.isFinestEnabled())
			tracer.finest("Nothing to do for SrsResourceAdaptor");		
	}

	@Override
	public void queryLiveness(final ActivityHandle handle) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}

	@Override
	public Object getActivity(final ActivityHandle handle) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
		return null;
	}

	@Override
	public ActivityHandle getActivityHandle(final Object activity) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
		return null;
	}

	@Override
	public void administrativeRemove(final ActivityHandle handle) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}

	@Override
	public void eventProcessingSuccessful(final ActivityHandle handle, final FireableEventType eventType, final Object event, final Address address, final ReceivableService service, final int flags) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
		
	}

	@Override
	public void eventProcessingFailed(final ActivityHandle handle, final FireableEventType eventType, final Object event, final Address address, final ReceivableService service, final int flags, final FailureReason reason) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}

	@Override
	public void eventUnreferenced(final ActivityHandle handle, final FireableEventType eventType, final Object event, final Address address, final ReceivableService service, final int flags) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}

	@Override
	public void activityEnded(final ActivityHandle handle) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}

	@Override
	public void activityUnreferenced(final ActivityHandle handle) {
		tracer.warning("SrsResourceAdaptor operates in client mode only");
	}
}
