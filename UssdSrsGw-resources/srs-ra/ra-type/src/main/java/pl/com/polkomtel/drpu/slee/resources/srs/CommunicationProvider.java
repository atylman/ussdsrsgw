package pl.com.polkomtel.drpu.slee.resources.srs;

import pl.com.polkomtel.drpu.slee.resources.srs.message.MessageFactory;

/** 
 * @slee.resource-adaptor-type
 *   id="${project.artifactId}"
 *   name="SRS Client RA"
 *   vendor="${ussdsrsgw.slee.vendor}"
 *   version="${project.version}"
 * @slee.resource-adaptor-interface
 *   id="${project.artifactId}"
 */
public interface CommunicationProvider {
	
	/**
	 * Gives an instance of message factory that enables to instantiate CORBA requests and responses
	 * @return message factory
	 */
	MessageFactory getMessageFactory();

	/**
	 * Gives an instance of a client object which enables to initiate requests over CORBA
	 * @return client instance
	 */
	Client getClient();
}