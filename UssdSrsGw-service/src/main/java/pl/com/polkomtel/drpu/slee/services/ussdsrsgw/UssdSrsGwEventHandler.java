package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import static javax.slee.facilities.TraceLevel.FINEST;
import static javax.slee.facilities.TraceLevel.INFO;

import javax.slee.ActivityContextInterface;

import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpResponse;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.alarm.UssdSrsGwAlarms;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event.UssdSrsGwEventCodes;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event.UssdSrsGwEventHelper;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.stats.UssdSrsGwUsageParameters;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.DefaultUssdRespMessages;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.GeneralUtils;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.GsmUtils;

import com.opencloud.slee.resources.cgin.Dialog;
import com.opencloud.slee.resources.cgin.DialogCloseEvent;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.DialogProviderAbortEvent;
import com.opencloud.slee.resources.cgin.DialogUserAbortEvent;
import com.opencloud.slee.resources.cgin.ProtocolException;
import com.opencloud.slee.resources.cgin.map.MAPDialog;
import com.opencloud.slee.resources.cgin.map.MAPUSSD_Arg;
import com.opencloud.slee.resources.cgin.map.events.MAPProcessUnstructuredSS_RequestRequestEvent;
import com.opencloud.slee.resources.in.datatypes.cc.AddressString;

/**
 * Defines methods for processing incoming events.
 * 
 * @author adam.tylman
 */
public abstract class UssdSrsGwEventHandler extends UssdSrsGwCmp {

    /**
	 * @slee.event-method
	 *	 initial-event="True"
	 *   event-type-name="com.opencloud.slee.resources.cgin.DialogOpenRequest"
	 *   event-type-vendor="OpenCloud"
	 *   event-type-version="1.5"
	 *   initial-event-select-variable="ActivityContext"
	 *   initial-event-selector-method-name="initialEventSelect"
	 */
	public void onOpenRequest(final DialogOpenRequestEvent event, final ActivityContextInterface aci) {
		MAPDialog dialog;
		String transactionId;
        
        try {
        	initializeTracing(getReporter().generateSessionTraceContext(UssdSrsGwSbb.TRACER_NAME));

        	if(isTraceable(FINEST))
    			finestf("onOpenRequest method: DialogOpenRequestEvent [%s]", event.toString());

        	dialog = (MAPDialog) event.getDialog();
            transactionId = GeneralUtils.bytesToHex(dialog.getRemoteTransactionID());
            setTransactionId(transactionId);
            setDialogAci(aci);
            setTimestamp(System.currentTimeMillis());
            
            final AddressString destRef = GsmUtils.extractMsisdnFromMapDialogPdu(event);
            if(destRef==null) {
            	publishEvent(UssdSrsGwEventCodes.PROCESSING_INIT, null);
            	publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, UssdSrsGwEventHelper.GENERAL_ERROR);
            	getDefaultSbbUsageParameterSet().incrementUssdErrors(1L);
            	severef("onOpenRequest method: destination reference primitive for MAP-OPEN service is not present: DialogOpenRequestEvent [%s]", event.toString());
            	raiseAlarm(UssdSrsGwAlarms.USSD_INCONSISTENT_DATA, transactionId, new Object[] {"MAP destination reference missing"});
            	detachLocalObject(aci);
            	return;
            }
            
            if(!normalizeCallingPartyNumber(destRef))
            	setOriginMsisdn(destRef.getAddress());
 
            publishEvent(UssdSrsGwEventCodes.PROCESSING_INIT, null);
            
            initializeSubscriberTracing(getOriginMsisdn());
            dialog.acceptDialog();
            
            if(isTraceable(INFO))
				infof("onOpenRequest method: MAP-OPEN successfully accepted for: CgPA [%s], TCAP OTID [%s]", destRef.getAddress(), transactionId);
        } catch (final Exception ex) {
        	detachLocalObject(aci);
        	warn("onOpenRequest method: error while processing MAP-OPEN", ex);
        	publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, ex.getMessage());
        	getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
        }
	}
	
	/**
	 * @slee.event-method
	 *	 initial-event="False"
	 *   event-type-name="com.opencloud.slee.resources.cgin.map.processUnstructuredSS_RequestRequest"
	 *   event-type-vendor="OpenCloud"
	 *   event-type-version="1.5"
	 */
    public void onProcessUnstructuredSSRequest(final MAPProcessUnstructuredSS_RequestRequestEvent event, final ActivityContextInterface aci) {
    	if(isTraceable(FINEST))
			finestf("onProcessUnstructuredSSRequest method: ProcessUnstructuredSsReqEvent [%s]", event.toString());
    	
    	getDefaultSbbUsageParameterSet().incrementProcessUnstSsReqReceived(1L);
    	
    	boolean isRespSent = false;
		try {
			final MAPUSSD_Arg ussdArg = event.getArgument();
			setUssdDataCodingScheme(ussdArg.getUssd_DataCodingScheme());
			setDialogInvokeId(event.getInvokeId());
			
			final String decodedUssdString = GsmUtils.decode(ussdArg.getUssd_String());
			if(!processUssdMsg(decodedUssdString)) {
				warnf("onProcessUnstructuredSSRequest method: missing params in the incoming USSD string [%s]", decodedUssdString);
				isRespSent = sendUssdResponse(DefaultUssdRespMessages.PARAMETER_MISSING, aci);
				if(isRespSent)
					publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, UssdSrsGwEventHelper.GENERAL_ERROR);
	            return;
			}

			final TopUpResponse resp = sendCorbaRequest(getVoucherId(), getRechargedMsisdn(), getOriginMsisdn());
			if(isTraceable(INFO))
				infof("onProcessUnstructuredSSRequest method: TopUp resp is received [%s]", resp);
			
			if(processCorbaResp(resp))
				publishEvent(UssdSrsGwEventCodes.PROCESSING_SUCCESSFULLY_ENDED, null);
			
		} catch (final Exception ex) {
			getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
			if(!isRespSent)
				handleException(
						"onProcessUnstructuredSSRequest method: error while processing ProcessUnstructuredSSReq",
						ex, UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR,
						DefaultUssdRespMessages.GENERAL_ERROR, aci);
		}
    }
    
    /**
 	 * @slee.event-method
 	 *	 initial-event="False"
 	 *   event-type-name="com.opencloud.slee.resources.cgin.DialogClose"
 	 *   event-type-vendor="OpenCloud"
 	 *   event-type-version="1.5"
 	 */
    public void onDialogClose(final DialogCloseEvent event, final ActivityContextInterface aci) {
    	recordSessionError("onDialogClose", event.getDialog());
    	super.onCloseRequest(event, aci);
    }
     
    /**
     * @slee.event-method
     *	 initial-event="False"
     *	 event-type-name="com.opencloud.slee.resources.cgin.DialogProviderAbort"
     *	 event-type-vendor="OpenCloud"
     *	 event-type-version="1.5"
 	 */
    public void onProviderAbort(final DialogProviderAbortEvent event, final ActivityContextInterface aci) {
    	recordSessionError("onProviderAbort", event.getDialog());
    	onAbort(aci);
    }
     
    /**
  	 * @slee.event-method
  	 *	 initial-event="False"
  	 *   event-type-name="com.opencloud.slee.resources.cgin.DialogUserAbort"
  	 *   event-type-vendor="OpenCloud"
  	 *   event-type-version="1.5"
  	 */
    public void onUserAbort(final DialogUserAbortEvent event, final ActivityContextInterface aci) {
    	recordSessionError("onUserAbort", event.getDialog());
    	onAbort(aci);
    }

    private void recordSessionError(final String src, final Dialog dialog) {
    	if(isTraceable(INFO))
			infof("%s method: unexpected USSD session error, dialog will be closed transparently", src);
    	publishEvent(UssdSrsGwEventCodes.USSD_TIMEOUT, String.format("TCAP OTID [%s]", GeneralUtils.bytesToHex(dialog.getRemoteTransactionID())));
    	getDefaultSbbUsageParameterSet().incrementUssdErrors(1L);
    }
    
    private void onAbort(final ActivityContextInterface aci) {
    	final MAPDialog mapDlg = (MAPDialog) aci.getActivity();
        try {
			mapDlg.sendClose(true);
		} catch (final ProtocolException ex) {
			warn("onAbort method: error while trying to close dialog transparently", ex);
		}
        detachLocalObject(aci);
	}
    
	/**
	 * Aquires {@link UssdSrsGwUsageParameters} instance used for stats updates
	 * @return interface to usage params instance
	 */
	public abstract UssdSrsGwUsageParameters getDefaultSbbUsageParameterSet();
	 
    protected abstract void publishEvent(UssdSrsGwEventCodes eventCode, String additionalInfo);
    protected abstract boolean normalizeCallingPartyNumber(AddressString msisdn);
    protected abstract boolean initializeSubscriberTracing(String tracedMsisdn);
    protected abstract boolean sendUssdResponse(String msg, ActivityContextInterface aci);
    protected abstract TopUpResponse sendCorbaRequest(String secretNumber, String rechargedMsisdn, String originMsisdn);
    protected abstract boolean processCorbaResp(TopUpResponse resp);
    protected abstract void handleException(String logMsg, Throwable ex, UssdSrsGwEventCodes eventCode, String ussdResp, ActivityContextInterface aci);
    protected abstract boolean processUssdMsg(String ussdMsg);
}
