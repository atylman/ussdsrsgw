package pl.com.polkomtel.drpu.slee.resources.srs;

import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.Tracer;

import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.IntHolder;
import org.omg.CORBA.NO_RESOURCES;
import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.omg.CORBA.ShortHolder;
import org.omg.CORBA.StringHolder;
import org.omg.CORBA.TIMEOUT;
import org.omg.CORBA.TRANSIENT;

import pl.com.polkomtel.drpu.slee.resources.srs.message.MessageFactory;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpRequest;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpResponse;
import pl.com.polkomtel.drpu.slee.resources.srs.message.ValidationResult;
import pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccess;
import pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError;

/**
 * An implementation of {@link Client} which actually implements communication layer for CORBA req/resp exchange to SRS.
 * 
 * @author adam.tylman
 */
public class ClientImpl implements Client {

	private final Tracer tracer;
	private final int maxRetries;
	private final MessageFactory messageFactory;
	private final ClientServerRef requestsPerformer;
	
	/**
	 * Default constructor
	 */
	public ClientImpl(final String corbaloc, final Tracer tracer, final AlarmFacility alarmFacility, final MessageFactory messageFactory, final int maxRetries) {
		super();
		this.maxRetries = maxRetries;
		this.tracer = tracer;
		this.messageFactory = messageFactory;
		this.requestsPerformer = new ClientServerRef(corbaloc, tracer, alarmFacility);
	}

	@Override
	public TopUpResponse performRequest(final TopUpRequest message) throws ClientRequestException {
		if (tracer.isFineEnabled()) {
			tracer.fine(String.format("performRequest: TopUpRequest being prepared [%s]", message));
		}
		
		int trialNumber = 0;
		TopUpResponse answer = null;
		Exception exception = null;
		final ShortHolder validationResult = new ShortHolder((short) ValidationResult.UNKNOWN_VALIDATION_RESULT_CODE.getResultCode());
		final IntHolder credit = new IntHolder();
		final StringHolder appProfile = new StringHolder();
		final StringHolder xmlout = new StringHolder();
		
		while(answer == null && trialNumber < maxRetries) {
			if (tracer.isFinestEnabled()) {
				tracer.finest(String.format("performRequest: TopUpRequest, performing attempt, trialNumber [%d] (starting from 0)", trialNumber));
			}

			if (requestsPerformer == null || !requestsPerformer.isValid()) {
				tracer.warning("performRequest: TopUpRequest, no valid ClientServerRef");
				throw new ClientRequestException("No valid client connection");
			}
			
			if (tracer.isFinestEnabled())
				tracer.finest(String.format("performRequest: TopUpRequest, got ClientServerRef [%s]", requestsPerformer.printStatus()));

			try {
				try {
					requestsPerformer.getServer().TopUp(message.getSecretNr(),
							message.getMsisdn(), message.getType(),
							message.getOriginator(), message.getChannel(),
							message.getXmlin(), validationResult, credit,
							appProfile, xmlout);
				} catch (TRANSIENT | OBJECT_NOT_EXIST | NO_RESOURCES | COMM_FAILURE ex) {
					tracer.warning("performRequest: general error for TopUp method invocation on ORB, possible retry", ex);
					trialNumber++;
					if(trialNumber >= maxRetries)
						requestsPerformer.markInvalid(ex);
					exception = ex;
					continue;
				} catch (TIMEOUT ex) {
					tracer.warning("performRequest: timeout error for TopUp method invocation on ORB, no retries", ex);
					trialNumber++;
					requestsPerformer.markInvalid(ex);
					exception = ex;
					break;
				}
				
				if (tracer.isFineEnabled())
					tracer.fine(String
							.format("performRequest: TopUpRequest, got an answer: valRes,cred,appProf,xmlout=[%d,%d,%s,%s]",
									validationResult.value, credit.value,
									appProfile.value, xmlout.value));
				answer = messageFactory.createTopUpResponseMessage(
							validationResult.value, credit.value, appProfile.value, xmlout.value);
				exception = null;
				
			} catch (NoAccess | SystemError e) {
				if(tracer.isInfoEnabled())
					tracer.info("performRequest: service specific error for TopUp method invocation on ORB, possible retry", e);
				answer = messageFactory.createTopUpResponseMessage(e);
			}
			
			trialNumber++;
		}
		
		if (answer == null)
			tracer.warning(String.format("performRequest: TopUpRequest, answer is null upon [%d] retries due to multiple general errors or timeout", trialNumber));

		if (exception != null)
			throw new ClientRequestException(exception);

		return answer;
	}
	
	@Override
	public void destroy() {
		requestsPerformer.clear();
		requestsPerformer.stopReinit();
	}
}
