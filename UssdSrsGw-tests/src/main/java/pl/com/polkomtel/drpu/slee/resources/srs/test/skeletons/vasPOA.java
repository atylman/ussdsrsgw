package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons;


/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public abstract class vasPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasOperations
{
	static private final java.util.HashMap<String,Integer> m_opsHash = new java.util.HashMap<String,Integer>();
	static
	{
		m_opsHash.put ( "TopUp", Integer.valueOf(0));
		m_opsHash.put ( "checkService", Integer.valueOf(1));
	}
	private String[] ids = {"IDL:vas:1.0"};
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas _this()
	{
		org.omg.CORBA.Object __o = _this_object() ;
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas __r = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasHelper.narrow(__o);
		if (__o != null && __o != __r)
		{
			((org.omg.CORBA.portable.ObjectImpl)__o)._set_delegate(null);

		}
		return __r;
	}
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas _this(org.omg.CORBA.ORB orb)
	{
		org.omg.CORBA.Object __o = _this_object(orb) ;
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas __r = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasHelper.narrow(__o);
		if (__o != null && __o != __r)
		{
			((org.omg.CORBA.portable.ObjectImpl)__o)._set_delegate(null);

		}
		return __r;
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // TopUp
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				short _arg2=_input.read_short();
				java.lang.String _arg3=_input.read_string();
				java.lang.String _arg4=_input.read_string();
				java.lang.String _arg5=_input.read_string();
				org.omg.CORBA.ShortHolder _arg6= new org.omg.CORBA.ShortHolder();
				org.omg.CORBA.IntHolder _arg7= new org.omg.CORBA.IntHolder();
				org.omg.CORBA.StringHolder _arg8= new org.omg.CORBA.StringHolder();
				org.omg.CORBA.StringHolder _arg9= new org.omg.CORBA.StringHolder();
				_out = handler.createReply();
				TopUp(_arg0,_arg1,_arg2,_arg3,_arg4,_arg5,_arg6,_arg7,_arg8,_arg9);
				_out.write_ushort(_arg6.value);
				_out.write_ulong(_arg7.value);
				java.lang.String tmpResult0 = _arg8.value;
_out.write_string( tmpResult0 );
				java.lang.String tmpResult1 = _arg9.value;
_out.write_string( tmpResult1 );
			}
			catch(pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError _ex0)
			{
				_out = handler.createExceptionReply();
				pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemErrorHelper.write(_out, _ex0);
			}
			catch(pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess _ex1)
			{
				_out = handler.createExceptionReply();
				pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.write(_out, _ex1);
			}
				break;
			}
			case 1: // checkService
			{
				org.omg.CORBA.ShortHolder _arg0= new org.omg.CORBA.ShortHolder();
				_out = handler.createReply();
				checkService(_arg0);
				_out.write_ushort(_arg0.value);
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
