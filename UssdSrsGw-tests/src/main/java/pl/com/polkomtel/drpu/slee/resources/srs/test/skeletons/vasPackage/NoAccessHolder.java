package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage;

/**
 * Generated from IDL exception "NoAccess".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public final class NoAccessHolder
	implements org.omg.CORBA.portable.Streamable
{
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess value;

	public NoAccessHolder ()
	{
	}
	public NoAccessHolder(final pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.write(_out, value);
	}
}
