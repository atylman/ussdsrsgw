package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage;

/**
 * Generated from IDL exception "NoAccess".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public final class NoAccess
	extends org.omg.CORBA.UserException
{
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	public NoAccess()
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.id());
	}

	public java.lang.String description = "";
	public NoAccess(java.lang.String _reason,java.lang.String description)
	{
		super(_reason);
		this.description = description;
	}
	public NoAccess(java.lang.String description)
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccessHelper.id());
		this.description = description;
	}
}
