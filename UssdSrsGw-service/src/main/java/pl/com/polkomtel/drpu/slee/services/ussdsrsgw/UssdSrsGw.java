package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

/**
 * Interface defined to encapsulate call context variables used to dump EDRs.
 * 
 * @author adam.tylman
 */
public interface UssdSrsGw {
	
	/**Standard getter method*/
	String getUssdCode();

	/**Standard getter method*/
	String getRechargedMsisdn();
	
	/**Standard getter method*/
	String getOriginMsisdn();

	/**Standard getter method*/
	String getVoucherId();

	/**Standard getter method*/
	int getSrsRespCode();

	/**Standard getter method*/
	String getAdditionalEventInfo();

	/**Standard getter method*/
	String getTransactionId();
	
	/**Standard getter method*/
	long getTimestamp();
}