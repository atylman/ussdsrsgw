package pl.com.polkomtel.drpu.slee.resources.srs.stubs;


/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public class _vasStub
	extends org.omg.CORBA.portable.ObjectImpl
	implements pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas
{
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	private String[] ids = {"IDL:vas:1.0"};
	public String[] _ids()
	{
		return ids;
	}

	public final static java.lang.Class _opsClass = pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasOperations.class;
	public void TopUp(java.lang.String secretNr, java.lang.String msisdn, short type, java.lang.String originator, java.lang.String channel, java.lang.String xmlin, org.omg.CORBA.ShortHolder validationResult, org.omg.CORBA.IntHolder credit, org.omg.CORBA.StringHolder appProfile, org.omg.CORBA.StringHolder xmlout) throws pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccess,pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError
	{
		while(true)
		{
			if(! this._is_local())
			{
				org.omg.CORBA.portable.InputStream _is = null;
				org.omg.CORBA.portable.OutputStream _os = null;
				try
				{
					_os = _request( "TopUp", true);
					java.lang.String tmpResult0 = secretNr;
_os.write_string( tmpResult0 );
					java.lang.String tmpResult1 = msisdn;
_os.write_string( tmpResult1 );
					_os.write_short(type);
					java.lang.String tmpResult2 = originator;
_os.write_string( tmpResult2 );
					java.lang.String tmpResult3 = channel;
_os.write_string( tmpResult3 );
					java.lang.String tmpResult4 = xmlin;
_os.write_string( tmpResult4 );
					_is = _invoke(_os);
					validationResult.value = _is.read_ushort();
					credit.value = _is.read_ulong();
					appProfile.value = _is.read_string();
					xmlout.value = _is.read_string();
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _rx )
					{
						continue;
					}
				catch( org.omg.CORBA.portable.ApplicationException _ax )
				{
					String _id = _ax.getId();
					try
					{
						if( _id.equals("IDL:vas/NoAccess:1.0"))
						{
							throw pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccessHelper.read(_ax.getInputStream());
						}
						else 
						if( _id.equals("IDL:vas/SystemError:1.0"))
						{
							throw pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemErrorHelper.read(_ax.getInputStream());
						}
						else 
						{
							throw new RuntimeException("Unexpected exception " + _id );
						}
					}
					finally
					{
						try
						{
							_ax.getInputStream().close();
						}
						catch (java.io.IOException e)
						{
							throw new RuntimeException("Unexpected exception " + e.toString() );
						}
					}
			}
			finally
			{
				if (_os != null)
				{
					try
					{
						_os.close();
					}
					catch (java.io.IOException e)
					{
						throw new RuntimeException("Unexpected exception " + e.toString() );
					}
				}
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "TopUp", _opsClass );
			if( _so == null )
				continue;
			vasOperations _localServant = (vasOperations)_so.servant;
			try
			{
				_localServant.TopUp(secretNr,msisdn,type,originator,channel,xmlin,validationResult,credit,appProfile,xmlout);
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).normalCompletion();
				return;
			}
			catch (pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccess ex) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(ex);
				throw ex;
			}
			catch (pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError ex) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(ex);
				throw ex;
			}
			catch (RuntimeException re) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(re);
				throw re;
			}
			catch (java.lang.Error err) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(err);
				throw err;
			}
			finally
			{
				_servant_postinvoke(_so);
			}
		}

		}

	}

	public void checkService(org.omg.CORBA.ShortHolder available)
	{
		while(true)
		{
			if(! this._is_local())
			{
				org.omg.CORBA.portable.InputStream _is = null;
				org.omg.CORBA.portable.OutputStream _os = null;
				try
				{
					_os = _request( "checkService", true);
					_is = _invoke(_os);
					available.value = _is.read_ushort();
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _rx )
					{
						continue;
					}
				catch( org.omg.CORBA.portable.ApplicationException _ax )
				{
					String _id = _ax.getId();
					try
					{
							_ax.getInputStream().close();
					}
					catch (java.io.IOException e)
					{
						throw new RuntimeException("Unexpected exception " + e.toString() );
					}
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				if (_os != null)
				{
					try
					{
						_os.close();
					}
					catch (java.io.IOException e)
					{
						throw new RuntimeException("Unexpected exception " + e.toString() );
					}
				}
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "checkService", _opsClass );
			if( _so == null )
				continue;
			vasOperations _localServant = (vasOperations)_so.servant;
			try
			{
				_localServant.checkService(available);
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).normalCompletion();
				return;
			}
			catch (RuntimeException re) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(re);
				throw re;
			}
			catch (java.lang.Error err) 
			{
				if ( _so instanceof org.omg.CORBA.portable.ServantObjectExt) 
					((org.omg.CORBA.portable.ServantObjectExt)_so).exceptionalCompletion(err);
				throw err;
			}
			finally
			{
				_servant_postinvoke(_so);
			}
		}

		}

	}

}
