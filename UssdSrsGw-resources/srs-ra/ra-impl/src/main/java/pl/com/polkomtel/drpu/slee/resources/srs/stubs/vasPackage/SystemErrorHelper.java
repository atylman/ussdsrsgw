package pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage;


/**
 * Generated from IDL exception "SystemError".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public final class SystemErrorHelper
{
	private volatile static org.omg.CORBA.TypeCode _type;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			synchronized(SystemErrorHelper.class)
			{
				if (_type == null)
				{
					_type = org.omg.CORBA.ORB.init().create_exception_tc(pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemErrorHelper.id(),"SystemError",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("method_name", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("description", org.omg.CORBA.ORB.init().create_string_tc(0), null)});
				}
			}
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError extract (final org.omg.CORBA.Any any)
	{
		org.omg.CORBA.portable.InputStream in = any.create_input_stream();
		try
		{
			return read (in);
		}
		finally
		{
			try
			{
				in.close();
			}
			catch (java.io.IOException e)
			{
			throw new RuntimeException("Unexpected exception " + e.toString() );
			}
		}
	}

	public static String id()
	{
		return "IDL:vas/SystemError:1.0";
	}
	public static pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError read (final org.omg.CORBA.portable.InputStream in)
	{
		String id = in.read_string();
		if (!id.equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id: " + id);
		java.lang.String x0;
		x0=in.read_string();
		java.lang.String x1;
		x1=in.read_string();
		final pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError result = new pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError(id, x0, x1);
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError s)
	{
		out.write_string(id());
		java.lang.String tmpResult5 = s.method_name;
out.write_string( tmpResult5 );
		java.lang.String tmpResult6 = s.description;
out.write_string( tmpResult6 );
	}
}
