package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;

import static org.powermock.api.mockito.PowerMockito.mock;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpResponse;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGwSbb;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.stats.UssdSrsGwUsageParameters;

import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;


public class UssdSrsGwSbbMock extends UssdSrsGwSbb {
	
	public UssdSrsGwSbbMock() {
		localSessionTraceContext = new SessionTraceContext("101-1422628754-49260", UssdSrsGwSbb.TRACER_NAME, false);
    }
	
	@Override
	protected TopUpResponse sendCorbaRequest(String secretNumber, String rechargedMsisdn, String originMsisdn) {
		return new TopUpResponse((short) 0, 10, "SIMPLUS", "");
	}
	
	@Override
	protected boolean sendUssdResponse(String msg, ActivityContextInterface aci) {
		return true;
	}

	@Override
	public UssdSrsGwUsageParameters getDefaultSbbUsageParameterSet() {
		return mock(UssdSrsGwUsageParameters.class);
	}

	@Override
	public SessionTraceContext getSessionTraceContext() {
		return localSessionTraceContext;
	}

	@Override
	public void setSessionTraceContext(SessionTraceContext traceContext) {
		localSessionTraceContext = traceContext;
	}

	@Override
	public ChildRelation getDbQuerySbbRelation() {
		return null;
	}

	@Override
	public ChildRelation getNumberNormalizerSbbRelation() {
		return null;
	}

	@Override
	public String getOriginMsisdn() {
		return null;
	}

	@Override
	public void setOriginMsisdn(String arg) {
	}

	@Override
	public long getTimestamp() {
		return 0;
	}

	@Override
	public void setTimestamp(long arg) {
	}

	@Override
	public String getTransactionId() {
		return null;
	}

	@Override
	public void setTransactionId(String arg) {
	}

	@Override
	public ActivityContextInterface getDialogAci() {
		return null;
	}

	@Override
	public void setDialogAci(ActivityContextInterface aci) {
	}
}
