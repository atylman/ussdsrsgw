package pl.com.polkomtel.drpu.slee.resources.srs.message;

import org.omg.CORBA.UserException;

/**
 * This interface provides factory methods that allow an application to create request and response
 * messages from a particular implementation of SRS RA.
 * 
 * @author adam.tylman
 */
public interface MessageFactory {
	
	/**
	 * Creates a new TopUp request message
	 * @return newly created request
	 */
	TopUpRequest createTopUpRequestMessage(String secretNr, String msisdn, short type, String originator, String channel, String xmlin);
	
	/**
	 * Creates a new TopUp valid response message
	 * @return newly created response
	 */
	TopUpResponse createTopUpResponseMessage(short validationResult, int credit, String appProfile, String xmlout);
	
	/**
	 * Creates a new TopUp response message as a result of an error
	 * @return newly created response
	 */
	TopUpResponse createTopUpResponseMessage(UserException userException);
}
