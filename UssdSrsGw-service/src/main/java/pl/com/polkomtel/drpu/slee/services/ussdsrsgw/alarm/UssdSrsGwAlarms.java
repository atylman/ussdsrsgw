package pl.com.polkomtel.drpu.slee.services.ussdsrsgw.alarm;

import javax.slee.facilities.AlarmLevel;

import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.UssdSrsGwSbb;

import com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm;

/**
 * Alarms used by {@link UssdSrsGwSbb}
 *
 * @author adam.tylman
 *
 */
public enum UssdSrsGwAlarms implements ServiceAlarm {

	INVALID_CONFIGURATION(AlarmLevel.MAJOR, "UssdSrsGwInvalidConfiguration", "UssdSrsGw service configuration problem: {0}", PARSEABLE),
	SRS_CONNECTION_ERROR(AlarmLevel.MAJOR, "CorbaConnectionError", "SRS connection problem: {0}", PARSEABLE),
	USSD_INCONSISTENT_DATA(AlarmLevel.MAJOR, "UssdInconsistentData", "USSD msg processing error: {0}", PARSEABLE);

	private final AlarmLevel alarmLevel;
	private final String alarmType;
	private final String messagePatttern;
	private boolean parseable = false;

	private UssdSrsGwAlarms(final AlarmLevel alarmLevel, final String alarmType, final String messagePattern, final boolean parseable) {
		this.alarmLevel = alarmLevel;
		this.alarmType = alarmType;
		this.messagePatttern = messagePattern;
		this.parseable = parseable;
	}

	/**
	 * Gets the alarm level
	 * @return the alarm level
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getAlarmLevel()
	 */
	@Override
	public AlarmLevel getAlarmLevel() {
		return this.alarmLevel;
	}

	/**
	 * Gets the alarm type
	 * @return the alarm type
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getAlarmType()
	 */
	@Override
	public String getAlarmType() {
		return this.alarmType;
	}

	/**
	 * Gets the alarm message pattern
	 * @return the alarm message pattern
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getMessagePattern()
	 */
	@Override
	public String getMessagePattern() {
		return this.messagePatttern;
	}

	/**
	 * Indicates if the alarm message is parseable or not
	 * @return <code>true</code> if alarm message is parseable, <code>false</code>
	 * otherwise
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#isParseable()
	 */
	@Override
	public boolean isParseable() {
		return this.parseable;
	}
}
