package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons;

import org.omg.PortableServer.POA;

/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public class vasPOATie
	extends vasPOA
{
	private vasOperations _delegate;

	private POA _poa;
	public vasPOATie(vasOperations delegate)
	{
		_delegate = delegate;
	}
	public vasPOATie(vasOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas _this()
	{
		org.omg.CORBA.Object __o = _this_object() ;
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas __r = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasHelper.narrow(__o);
		if (__o != null && __o != __r)
		{
			((org.omg.CORBA.portable.ObjectImpl)__o)._set_delegate(null);

		}
		return __r;
	}
	public pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas _this(org.omg.CORBA.ORB orb)
	{
		org.omg.CORBA.Object __o = _this_object(orb) ;
		pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vas __r = pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasHelper.narrow(__o);
		if (__o != null && __o != __r)
		{
			((org.omg.CORBA.portable.ObjectImpl)__o)._set_delegate(null);

		}
		return __r;
	}
	public vasOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(vasOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		return super._default_POA();
	}
	public void TopUp(java.lang.String secretNr, java.lang.String msisdn, short type, java.lang.String originator, java.lang.String channel, java.lang.String xmlin, org.omg.CORBA.ShortHolder validationResult, org.omg.CORBA.IntHolder credit, org.omg.CORBA.StringHolder appProfile, org.omg.CORBA.StringHolder xmlout) throws pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError,pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess
	{
_delegate.TopUp(secretNr,msisdn,type,originator,channel,xmlin,validationResult,credit,appProfile,xmlout);
	}

	public void checkService(org.omg.CORBA.ShortHolder available)
	{
_delegate.checkService(available);
	}

}
