package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons;


/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public interface vasOperations
{
	/* constants */
	/* operations  */
	void TopUp(java.lang.String secretNr, java.lang.String msisdn, short type, java.lang.String originator, java.lang.String channel, java.lang.String xmlin, org.omg.CORBA.ShortHolder validationResult, org.omg.CORBA.IntHolder credit, org.omg.CORBA.StringHolder appProfile, org.omg.CORBA.StringHolder xmlout) throws pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.SystemError,pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons.vasPackage.NoAccess;
	void checkService(org.omg.CORBA.ShortHolder available);
}
