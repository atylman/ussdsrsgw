package pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage;

/**
 * Generated from IDL exception "SystemError".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public final class SystemError
	extends org.omg.CORBA.UserException
{
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	public SystemError()
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemErrorHelper.id());
	}

	public java.lang.String method_name = "";
	public java.lang.String description = "";
	public SystemError(java.lang.String _reason,java.lang.String method_name, java.lang.String description)
	{
		super(_reason);
		this.method_name = method_name;
		this.description = description;
	}
	public SystemError(java.lang.String method_name, java.lang.String description)
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemErrorHelper.id());
		this.method_name = method_name;
		this.description = description;
	}
}
