package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import javax.slee.ActivityContextInterface;

import com.nsn.ploc.feniks.slee.nsnbaselib.BaseTCAPSbb;
import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;

/**
 * Defines both cmp and non-cmp SBB variables used for service logic and EDR generation.
 * 
 * @author adam.tylman
 */
public abstract class UssdSrsGwCmp extends BaseTCAPSbb {
	
	enum Mode {STANDARD, ON_BEHALF, UNSPECIFIED};
	
	private String ussdCode = null;
	private String rechargedMsisdn = null;
	private String voucherId = null;
	private int srsRespCode = 1000;
	private int dialogInvokeId = 0;
	private byte[] ussdDataCodingScheme = null;
	private String additionalEventInfo = null;
	private Mode rechargeMode = Mode.UNSPECIFIED;

	
	/***********************************************************************
	 **************************** CMP variables ****************************
	 ***********************************************************************/
	
	/**
     * Getter for cmp field
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract String getOriginMsisdn();
    
    /**
     * Setter for cmp field
     * @param arg cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setOriginMsisdn(String arg);

	/**
     * Getter for cmp field
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract long getTimestamp();
    
    /**
     * Setter for cmp field
     * @param arg cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setTimestamp(long arg);
	
    /**
     * Getter for cmp field
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract String getTransactionId();
    
    /**
     * Setter for cmp field
     * @param arg cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setTransactionId(String arg);
	
    /**
     * Getter for cmp field
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract ActivityContextInterface getDialogAci();
    
    /**
     * Setter for cmp field
     * @param arg cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setDialogAci(ActivityContextInterface arg);
    
    /**
     * Getter for cmp field, must override a corresponding {@link com.nsn.ploc.feniks.slee.nsnbaselib.BaseSbb#getSessionTraceContext()}
     * in order to make it visible for local XDoclet
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
	public abstract SessionTraceContext getSessionTraceContext();

    /**
     * Setter for cmp field, must override a corresponding {@link com.nsn.ploc.feniks.slee.nsnbaselib.BaseSbb#setSessionTraceContext(SessionTraceContext traceContext)}
     * in order to make it visible for local XDoclet 
     * @param arg cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
	public abstract void setSessionTraceContext(SessionTraceContext arg);

	
	/************************************************************************
	 **************** Local variables used for service logic ****************
	 ************************************************************************/
	
	/**
	 * @return the ussdDataCodingScheme
	 */
	byte[] getUssdDataCodingScheme() {
		if(ussdDataCodingScheme!=null)
			return ussdDataCodingScheme.clone();
		else
			return null;
	}

	/**
	 * @param ussdDataCodingScheme the ussdDataCodingScheme to set
	 */
	void setUssdDataCodingScheme(final byte[] ussdDataCodingScheme) {
		this.ussdDataCodingScheme = ussdDataCodingScheme.clone();
	}
	
	/**
	 * @return the dialogInvokeId
	 */
	int getDialogInvokeId() {
		return dialogInvokeId;
	}

	/**
	 * @param dialogInvokeId the dialogInvokeId to set
	 */
	void setDialogInvokeId(final int dialogInvokeId) {
		this.dialogInvokeId = dialogInvokeId;
	}
	
	/**
	 * @return the rechargeMode
	 */
	Mode getRechargeMode() {
		return rechargeMode;
	}

	/**
	 * @param rechargeMode the rechargeMode to set
	 */
	void setRechargeMode(final Mode rechargeMode) {
		this.rechargeMode = rechargeMode;
	}


	/***********************************************************************
	 ******************** Local variables used for EDRs ********************
	 ***********************************************************************/
	
	/**
	 * @return the ussdCode
	 */
	public String getUssdCode() {
		return ussdCode;
	}

	/**
	 * @param ussdCode the ussdCode to set
	 */
	void setUssdCode(final String ussdCode) {
		this.ussdCode = ussdCode;
	}

	/**
	 * @return the rechargedMsisdn
	 */
	public String getRechargedMsisdn() {
		return rechargedMsisdn;
	}

	/**
	 * @param rechargedMsisdn the rechargedMsisdn to set
	 */
	void setRechargedMsisdn(final String rechargedMsisdn) {
		this.rechargedMsisdn = rechargedMsisdn;
	}

	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherId the voucherId to set
	 */
	void setVoucherId(final String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the srsRespCode
	 */
	public int getSrsRespCode() {
		return srsRespCode;
	}

	/**
	 * @param srsRespCode the srsRespCode to set
	 */
	void setSrsRespCode(final int srsRespCode) {
		this.srsRespCode = srsRespCode;
	}

	/**
	 * @return the additionalEventInfo
	 */
	public String getAdditionalEventInfo() {
		return additionalEventInfo;
	}

	/**
	 * @param additionalEventInfo the additionalEventInfo to set
	 */
	void setAdditionalEventInfo(final String additionalEventInfo) {
		this.additionalEventInfo = additionalEventInfo;
	}
}
