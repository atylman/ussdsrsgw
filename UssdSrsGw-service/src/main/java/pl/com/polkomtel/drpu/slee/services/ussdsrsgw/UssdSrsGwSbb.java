package pl.com.polkomtel.drpu.slee.services.ussdsrsgw;

import static com.nsn.ploc.feniks.slee.nsnbaselib.dispatcher.INAPDPQueryHelper.getQryCheckSubscriberTrace;
import static javax.slee.facilities.TraceLevel.FINE;
import static javax.slee.facilities.TraceLevel.FINEST;
import static javax.slee.facilities.TraceLevel.INFO;

import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.InitialEventSelector;
import javax.slee.InvalidArgumentException;
import javax.slee.SbbContext;

import org.apache.commons.lang.StringUtils;

import pl.com.polkomtel.common.model.NumberNormalizerServiceType;
import pl.com.polkomtel.drpu.slee.resources.srs.Client;
import pl.com.polkomtel.drpu.slee.resources.srs.ClientRequestException;
import pl.com.polkomtel.drpu.slee.resources.srs.CommunicationProvider;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpRequest;
import pl.com.polkomtel.drpu.slee.resources.srs.message.TopUpResponse;
import pl.com.polkomtel.drpu.slee.resources.srs.message.ValidationResult;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.alarm.UssdSrsGwAlarms;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event.UssdSrsGwEventCodes;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.event.UssdSrsGwEventHelper;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.DefaultUssdRespMessages;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.GeneralUtils;
import pl.com.polkomtel.drpu.slee.services.ussdsrsgw.utils.GsmUtils;

import com.nsn.ploc.feniks.slee.common.db.DbQuerySbb;
import com.nsn.ploc.feniks.slee.common.db.DbQuerySbbLocalInterface;
import com.nsn.ploc.feniks.slee.common.numbernormalizer.NumberNormalizerResult;
import com.nsn.ploc.feniks.slee.common.numbernormalizer.NumberNormalizerSbb;
import com.nsn.ploc.feniks.slee.common.numbernormalizer.NumberNormalizerSbbLocalInterface;
import com.nsn.ploc.feniks.slee.common.numbernormalizer.consts.NumberNormalizerResultCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.alarm.CommonServiceAlarms;
import com.nsn.ploc.feniks.slee.nsnbaselib.db.DbBooleanResult;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.map.MAPDialog;
import com.opencloud.slee.resources.cgin.map.MAPUSSD_Arg;
import com.opencloud.slee.resources.cgin.map.MAPUSSD_Res;
import com.opencloud.slee.resources.cgin.map.events.MAPProcessUnstructuredSS_RequestRequestEvent;
import com.opencloud.slee.resources.cgin.map.metadata.MAPApplicationContexts;
import com.opencloud.slee.resources.in.datatypes.cc.AddressString;

/**
 * @slee.service
 *   name="${project.root.name}Service"
 *   vendor="${ussdsrsgw.slee.vendor}"
 *   version="${project.version}"
 *   default-priority="99"
 * @slee.sbb
 *   id="${project.root.name}Sbb"
 *   name="${project.root.name}Sbb"
 *   vendor="${ussdsrsgw.slee.vendor}"
 *   version="${project.version}"
 *   sbb-usage-parameters-interface-name="pl.com.polkomtel.drpu.slee.services.ussdsrsgw.stats.UssdSrsGwUsageParameters"
 * @slee.sbb-ref
 *   sbb-name="NumberNormalizerSbb"
 *   sbb-vendor="NSN"
 *   sbb-version="${numbernormalizer.slee.version}"
 *   sbb-alias="NumberNormalizerSbb"
 * @slee.sbb-ref
 *   sbb-name="DbQuerySbb"
 *   sbb-vendor="NSN"
 *   sbb-version="${dbquery.slee.version}"
 *   sbb-alias="DbQuerySbb"
 * @slee.library-ref
 *   library-name="Polkomtel ARP Base Library"
 *   library-vendor="CTS"
 *   library-version="${plkbase.slee.version}"
 * @slee.library-ref
 *   library-name="EventReporter"
 *   library-vendor="NSN"
 *   library-version="${eventreporter.slee.version}"
 * @slee.library-ref
 *   library-name="CGIN common"
 *   library-vendor="OpenCloud"
 *   library-version="${cgincommon.slee.version}"
 * @slee.env-entry
 *   env-entry-name="ussdAcceptableCodes"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="${ussdsrsgw.env.ussdacceptablecodes}"
 * @slee.env-entry
 *   env-entry-name="corbaTopUpReqChannelParam"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="VUSSD"
 * @slee.env-entry
 *   env-entry-name="corbaTopUpReqTypeParam"
 *   env-entry-type="java.lang.Short"
 *   env-entry-value="0"
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="EventReporter"
 *   resource-adaptor-type-vendor="NSN"
 *   resource-adaptor-type-version="${eventreporter.slee.version}"
 *   resource-adaptor-object-name="slee/resources/er/provider"
 *   resource-adaptor-entity-link="${eventreporter.raentity.link}"
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="MAP"
 *   resource-adaptor-type-vendor="OpenCloud"
 *   resource-adaptor-type-version="${cginmap.slee.version}"
 *   resource-adaptor-object-name="slee/resources/cgin/map/provider"
 *   resource-adaptor-entity-link="${cginmap.raentity.link}"
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="SRS Client RA"
 *   resource-adaptor-type-vendor="Polkomtel"
 *   resource-adaptor-type-version="${srs.slee.version}"
 *   resource-adaptor-object-name="slee/resources/srs/provider"
 *   resource-adaptor-entity-link="${srs.raentity.link}"
 */
public abstract class UssdSrsGwSbb extends UssdSrsGwEventHandler implements UssdSrsGw {
	
	static final String TRACER_NAME = "plk.ussdsrsgw";
	
	private static final String ENV_ENTRY_USSD_ACCEPTABLE_CODES = "ussdAcceptableCodes";
	private static final String ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM = "corbaTopUpReqChannelParam";
	private static final String ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM = "corbaTopUpReqTypeParam";
	private static final String SRS_PROVIDER_JNDI_NAME = "slee/resources/srs/provider";
	
	private static final String ENV_ENTRY_USSD_ACCEPTABLE_CODES_REGEX = "^[1-9][0-9]{2}(,[1-9][0-9]{2})*$";
	private static final String USSD_MSG_FOR_STANDARD_MODE_REGEX = "^\\*[1-9][0-9]{2}\\*[A-Za-z0-9]{14,16}#$";
	private static final String USSD_MSG_FOR_ON_BEHALF_MODE_REGEX = "^\\*[1-9][0-9]{2}\\*[A-Za-z0-9]{14,16}\\*[1-9][0-9]{8,10}#$";
	private static final Pattern ENV_ENTRY_USSD_ACCEPTABLE_CODES_PATTERN = Pattern.compile(ENV_ENTRY_USSD_ACCEPTABLE_CODES_REGEX);
	private static final Pattern USSD_MSG_FOR_STANDARD_MODE_PATTERN = Pattern.compile(USSD_MSG_FOR_STANDARD_MODE_REGEX);
	private static final Pattern USSD_MSG_FOR_ON_BEHALF_MODE_PATTERN = Pattern.compile(USSD_MSG_FOR_ON_BEHALF_MODE_REGEX);

	private String [] ussdAcceptableCodes = null;
	private String corbaTopUpReqChannelParam = "";
	private short corbaTopUpReqTypeParam = (short) 0;

	private CommunicationProvider corbaProvider = null;

	@Override
	public void setSbbContext(final SbbContext context) {
		try {
			super.setSbbContext(context);
			
			//localSessionTraceContext of BaseSbb class must be initialized first before any raiseAlarm method is invoked
			initializeTracing(getReporter().generateSessionTraceContext(TRACER_NAME));

			final Context env = (Context) new InitialContext().lookup("java:comp/env");
			
			corbaProvider = (CommunicationProvider) env.lookup(SRS_PROVIDER_JNDI_NAME);
			setEnvVariables(env);
			clearAlarms(UssdSrsGwAlarms.INVALID_CONFIGURATION.getAlarmType());

		} catch (final NamingException ne) {
			final String msg = "Error while getting environment entries or reference to environment: "+ne.getMessage();
			severe(msg, ne);
			throw new RuntimeException(msg, ne);
		} catch (final InvalidArgumentException iae) {
			final String msg = "Invalid configuration, failed to validate environment variables: "+iae.getMessage();
			severe(msg, iae);
			raiseAlarm(UssdSrsGwAlarms.INVALID_CONFIGURATION, CommonServiceAlarms.EMPTY_ALARM_ID, new Object[] {iae.getMessage()});
			throw new RuntimeException(msg, iae);
		}
	}

	/**
	 * Reads environment entries and initializes SBB instance variables.
	 *
	 * @param env the naming context
	 * @throws NamingException if environment variable is not found
	 * @throws InvalidArgumentException if environment variable's value is invalid
	 */
	private void setEnvVariables(final Context env) throws NamingException, InvalidArgumentException {
	
		final String tempUssdAcceptableCodes = (String) env.lookup(ENV_ENTRY_USSD_ACCEPTABLE_CODES);
		final String tempCorbaTopUpReqChannelParam = (String) env.lookup(ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM);
		final Short tempCorbaTopUpReqTypeParam = (Short) env.lookup(ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM);

		if(getSbbTracer().isFinestEnabled())
			finestf("The following env entries are read: %s [%s], %s [%s], %s [%s]",
					ENV_ENTRY_USSD_ACCEPTABLE_CODES, tempUssdAcceptableCodes,
					ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM, tempCorbaTopUpReqChannelParam, 
					ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM, tempCorbaTopUpReqTypeParam);

		if(!GeneralUtils.isPatternMatched(tempUssdAcceptableCodes, ENV_ENTRY_USSD_ACCEPTABLE_CODES_PATTERN))
			throw new InvalidArgumentException(
					String.format("The [%s] is missing or doesn't match [%s] pattern",
					ENV_ENTRY_USSD_ACCEPTABLE_CODES, ENV_ENTRY_USSD_ACCEPTABLE_CODES_PATTERN));
		
		if(StringUtils.isBlank(tempCorbaTopUpReqChannelParam) || tempCorbaTopUpReqTypeParam == null)
			throw new InvalidArgumentException(
					String.format(
							"The [%s] cannot be empty",
							tempCorbaTopUpReqTypeParam == null ? ENV_ENTRY_CORBA_TOP_UP_TYPE_PARAM : ENV_ENTRY_CORBA_TOP_UP_CHANNEL_PARAM));
		
		ussdAcceptableCodes = tempUssdAcceptableCodes.split(",");
		corbaTopUpReqChannelParam = tempCorbaTopUpReqChannelParam;
		corbaTopUpReqTypeParam = tempCorbaTopUpReqTypeParam.shortValue();
	}
	
	/**
	 * Default SLEE InitialEventSelector method used for filtering out USSD traffic of our interest.
	 * 
	 * @param ies SLEE {@link InitialEventSelector}
	 * @return {@link InitialEventSelector} object after customization
	 */
	public InitialEventSelector initialEventSelect(final InitialEventSelector ies) {
		try {
			if (ies.getEvent() instanceof DialogOpenRequestEvent) {
				final DialogOpenRequestEvent openRequest = (DialogOpenRequestEvent) ies.getEvent();
				if(getSbbTracer().isFinestEnabled())
					finestf("IES method: IES.address [%s], DialogOpenRequestEvent [%s]", ies.getAddress(), openRequest);
				
				if (openRequest.getApplicationContext() == MAPApplicationContexts.networkUnstructuredSsContext_v2_ac) {
					if(isUssdCodeMatched(openRequest))
						ies.setInitialEvent(true);
					else
						ies.setInitialEvent(false);
				} else {
					if(getSbbTracer().isFinestEnabled())
						finestf("IES method: invalid ACN [%s]", openRequest.getApplicationContext().toString());
					ies.setInitialEvent(false);
				}
			}
		} catch(final Exception ex) {
			warn("IES method: exception thrown", ex);
			ies.setInitialEvent(false);
		}

		return ies;
	}
	
	/**
	 * Verifies if incoming USSD code should be handled by our service.
	 *
	 * @param openRequest initial event containing USSD string
	 * @return true if USSD code matches a pattern
	 */
	private boolean isUssdCodeMatched(final DialogOpenRequestEvent openRequest) {
		
		if (openRequest.getComponentEvents().length>=0 && openRequest.getComponentEvents()[0] instanceof MAPProcessUnstructuredSS_RequestRequestEvent) {
			final MAPProcessUnstructuredSS_RequestRequestEvent requestEvent = (MAPProcessUnstructuredSS_RequestRequestEvent) openRequest.getComponentEvents()[0];
			final MAPUSSD_Arg ussdArg = requestEvent.getArgument();
			String decodedUssdString = GsmUtils.decode(ussdArg.getUssd_String());
			decodedUssdString = GeneralUtils.extractUssdMsgParam(decodedUssdString, 0);
			
			if(getSbbTracer().isFineEnabled())
				finef("Check USSD code matching: decoded string [%s]", decodedUssdString);

			for (final String acceptableCode : ussdAcceptableCodes)
				if (decodedUssdString.equals(acceptableCode)) {
					if(getSbbTracer().isFinestEnabled())
						finestf("USSD code matched for [%s]", acceptableCode);
					return true;
				}
		} else {
			severe(String.format("Check USSD code matching: unexpected event detected in DialogOpenRequest [%s]",
							openRequest.getComponentEvents().length >= 0 ? openRequest.getComponentEvents()[0] : "no event"));
			return false;
		}
		
		return false;
	}
	
	@Override
	protected boolean sendUssdResponse(final String msg, final ActivityContextInterface aci) {
		if(isTraceable(FINEST))
			finestf("sendUssdResponse method: preparing ProcessUnstructuredSS confirmation containg msg [%s]", msg);

		try {
			final byte[] encodedUssdStr = GsmUtils.encode(msg);

            final MAPUSSD_Res resp = new MAPUSSD_Res();
            resp.setUssd_String(encodedUssdStr);
            final byte[] dataCoding = getUssdDataCodingScheme();
            if(dataCoding != null)
            	resp.setUssd_DataCodingScheme(dataCoding);
            else
            	resp.setUssd_DataCodingScheme(new byte[] {GsmUtils.DEFAULT_DATA_CODING_SCHEME});

            final MAPDialog mapDlg = (MAPDialog) aci.getActivity();
            mapDlg.sendProcessUnstructuredSS_RequestResponse(getDialogInvokeId(), resp);
            mapDlg.sendClose(false);
            getDefaultSbbUsageParameterSet().incrementProcessUnstSsRespSent(1L);
        } catch (final Exception ex) {
        	detachLocalObject(aci);
        	warn("sendUssdResponse method: error while sending ProcessUnstructuredSS confirmation", ex);
            publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, ex.getMessage());
            getDefaultSbbUsageParameterSet().incrementUssdErrors(1L);
            return false;
        }
		
		detachLocalObject(aci);
		return true;
	}
	
	@Override
	protected void handleException(final String logMsg, final Throwable exception, final UssdSrsGwEventCodes eventCode, final String ussdResp, final ActivityContextInterface aci) {
		warn(logMsg, exception);
		if(sendUssdResponse(ussdResp, aci))
			publishEvent(eventCode, exception.getMessage());
	}
	
	@Override
	protected boolean processUssdMsg(final String ussdMsg) {
		String ussdCode;
		String voucherCode;
		String rechargedParty;

		if(GeneralUtils.isPatternMatched(ussdMsg, UssdSrsGwSbb.USSD_MSG_FOR_STANDARD_MODE_PATTERN))
			setRechargeMode(Mode.STANDARD);
		else if(GeneralUtils.isPatternMatched(ussdMsg, UssdSrsGwSbb.USSD_MSG_FOR_ON_BEHALF_MODE_PATTERN))
			setRechargeMode(Mode.ON_BEHALF);
		else {
			if(isTraceable(INFO))
				infof("USSD string [%s] does not comply with any of allowed patterns: [%s] or [%s]",
						ussdMsg, USSD_MSG_FOR_STANDARD_MODE_PATTERN, USSD_MSG_FOR_ON_BEHALF_MODE_PATTERN);
			return false;
		}
		
		ussdCode = GeneralUtils.extractUssdMsgParam(ussdMsg, 0);
		voucherCode = GeneralUtils.extractUssdMsgParam(ussdMsg, 1);
		
		if(getRechargeMode() == Mode.ON_BEHALF) {
			rechargedParty = GeneralUtils.extractUssdMsgParam(ussdMsg, 2);
			if(rechargedParty.length() == GeneralUtils.SHORT_MSISDN_LENGTH)
				rechargedParty = GeneralUtils.DEFAULT_PREFIX.concat(rechargedParty);
		} else
			rechargedParty = getOriginMsisdn();

		if(isTraceable(INFO))
			infof("processUssdMsg method: params extracted from USSD string: entire string [%s], recharge mode [%s], USSD code [%s], voucher [%s], recharged MSISDN [%s]",
					ussdMsg, getRechargeMode().name(), ussdCode, voucherCode, rechargedParty);
		
		setUssdCode(ussdCode);
		setVoucherId(voucherCode);
		setRechargedMsisdn(rechargedParty);
		
		if(StringUtils.isBlank(ussdCode) || StringUtils.isBlank(voucherCode) || StringUtils.isBlank(rechargedParty))
			return false;
		else
			return true;
	}
	
	@Override
	protected TopUpResponse sendCorbaRequest(final String secretNumber, final String rechargedMsisdn, final String originMsisdn) {
		TopUpResponse resp = null;
		Exception exceptionThrown = null;
		
		try {
			final Client client = corbaProvider.getClient();
			final TopUpRequest req = corbaProvider.getMessageFactory().createTopUpRequestMessage(
															secretNumber, rechargedMsisdn,
															corbaTopUpReqTypeParam, originMsisdn,
															corbaTopUpReqChannelParam,
															GeneralUtils.EMPTY_STRING);
			if(isTraceable(FINE))
				finef("sendCorbaRequest method: TopUp req is ready [%s]", req.toString());
			
			final long reqStartTime = System.currentTimeMillis();
			resp = client.performRequest(req);
			getDefaultSbbUsageParameterSet().sampleSrsQueryTime(System.currentTimeMillis() - reqStartTime);
			
			publishEvent(UssdSrsGwEventCodes.SRS_REQUEST_SENT, null);
			getDefaultSbbUsageParameterSet().incrementSrsReqSent(1L);

		} catch(final ClientRequestException ex) {
			exceptionThrown = ex;
		}
		
		if(exceptionThrown != null || (exceptionThrown = resp.getUserException()) != null) {
			handleException(
					"sendCorbaRequest method: error while sending TopUp req", exceptionThrown,
					UssdSrsGwEventCodes.SRS_COMMUNICATION_FAILURE,
					DefaultUssdRespMessages.TOP_UP_ERROR, getDialogAci());
			getDefaultSbbUsageParameterSet().incrementSrsErrors(1L);
			return null;
		} else
			return resp;
	}
	
	@Override
	protected boolean processCorbaResp(final TopUpResponse resp) {
		if(resp == null) {
			raiseAlarm(UssdSrsGwAlarms.SRS_CONNECTION_ERROR, String.format("resource-adaptor-object-name:%s", SRS_PROVIDER_JNDI_NAME), new Object[] {"TopUp req sending error"});
			return false;
		}
		else
			clearAlarms(UssdSrsGwAlarms.SRS_CONNECTION_ERROR.getAlarmType());
		
		final ValidationResult respRes = resp.getValidationResult();
		setSrsRespCode(respRes.getResultCode());
		getDefaultSbbUsageParameterSet().incrementSrsRespReceived(1L);
		if(respRes == ValidationResult.VALIDATION_SUCCESSFUL)
			publishEvent(UssdSrsGwEventCodes.SRS_RESPONSE_OK, null);
		else
			publishEvent(UssdSrsGwEventCodes.SRS_RESPONSE_NOK, resp.toString());
		
		return sendUssdResponse(respRes.getDescription(), getDialogAci());
	}
		
	/**
	 * Verify if USSD sender is traceable and if so, set localSessionTraceContext accordingly.
	 * 
	 * @param tracedMsisdn number to be checked
	 * @return <code>true</code> if subscriber is supposed to be traced or <code>false</code> otherwise
	 */
	@Override
	protected boolean initializeSubscriberTracing(final String tracedMsisdn) {
		if (isTraceable(FINEST))
			finestf("initializeSubscriberTracing method: traced number [%s]", tracedMsisdn);

		if (tracedMsisdn == null) {
			warn("initializeSubscriberTracing method: MSISDN cannot be null, tracing status will not be determined.");
			return false;
		}
		
		try {
			final DbQuerySbbLocalInterface dbQuery = getDbQuerySbb();
			if (dbQuery == null) {
	        	publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, "Unable to create DbQuery child SBB");
	        	getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
	        	warn("initializeSubscriberTracing method: unable to create DbQuery child SBB");
				return false;
			}
			
			//localSessionTraceContext is not saved in cmp yet so getSessionTraceContext() method would return null
			final DbBooleanResult traceCheckResult = dbQuery.getNumberBooleanValue(localSessionTraceContext, getQryCheckSubscriberTrace(), tracedMsisdn);
			
			if(traceCheckResult != null && traceCheckResult.isSuccess()) {
				if (Boolean.TRUE.equals(traceCheckResult.getResult())) {
					localSessionTraceContext.setSubscriberTraceable(true);
					if (isTraceable(FINE))
						finef("initializeSubscriberTracing method: subscriber number tracing is enabled for [%s]", tracedMsisdn);
				} else {
					if (isTraceable(FINE))
						finef("initializeSubscriberTracing method: subscriber number tracing is disabled for [%s]", tracedMsisdn);
				}
			} else {
				publishEvent(UssdSrsGwEventCodes.DB_ACCESS_NGIN_ERROR, "DbBooleanResult invalid or not available");
				getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
				warnf("initializeSubscriberTracing method: db query failed for [%s] with result [%s]",
						tracedMsisdn, traceCheckResult==null ? "null" : traceCheckResult.toString());
				return false;
			}
		} catch (final Exception ex) {
			warn(String.format(
					"initializeSubscriberTracing method: error while enabling subscriber tracing for [%s]",
					tracedMsisdn), ex);
			publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR,
					String.format("Subscriber tracing initialization failed by throwing an exception [%s]", ex.getMessage()));
			getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
		}

		return false;
	}

	/**
	 * Normalizes USSD sender MSISDN number
	 * 
	 * @param msisdn destination reference number
	 * @return <code>true</code> if normalization succeeded or <code>false</code> otherwise
	 */
	@Override
	protected boolean normalizeCallingPartyNumber(final AddressString msisdn) {
		
		try {
	        final NumberNormalizerSbbLocalInterface numberNormalizerSbb = getNumberNormalizerSbb();
	        if (numberNormalizerSbb == null) {
	        	publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, "Unable to create NumberNormalizer child SBB");
	        	getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
	        	warn("normalizeCallingPartyNumber method: unable to create NumberNormalizer child SBB");
				return false;
			}

	        //localSessionTraceContext is not saved in cmp yet so getSessionTraceContext() method would return null
			final NumberNormalizerResult numberNormalizerResult = numberNormalizerSbb.normalizeNumber(localSessionTraceContext,
							msisdn.getAddress(),
							msisdn.getNature().intValue(),
							null, 0, NumberNormalizerServiceType.uss.toString());

			if (numberNormalizerResult != null && numberNormalizerResult.getNumberNormalizerResultCode().equals(NumberNormalizerResultCode.NUMBER_FORMATTED)) {
				if (isTraceable(FINEST))
					finestf("normalizeCallingPartyNumber method: normalization was successfull with CgPA equal to [%s]", numberNormalizerResult.getFormattedCallingPartyNumber());
				setOriginMsisdn(numberNormalizerResult.getFormattedCallingPartyNumber());
				return true;
			} else {
				publishEvent(UssdSrsGwEventCodes.NUMBER_NORMALIZER_NGIN_ERROR, "NumberNormalizerResult invalid or not available");
				getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
				warnf("normalizeCallingPartyNumber method: normalization failed for [%s] with result [%s]",
						msisdn.getAddress(), numberNormalizerResult==null ? "null" : numberNormalizerResult.getNumberNormalizerResultCode());
				return false;
			}
		} catch (final Exception ex) {
			warn(String.format("normalizeCallingPartyNumber method: error during normalization of msisdn [%s]", msisdn.getAddress()), ex);
			publishEvent(UssdSrsGwEventCodes.INTERNAL_SERVICE_ERROR, String.format("Normalization failed by throwing an exception [%s]", ex.getMessage()));
			getDefaultSbbUsageParameterSet().incrementGeneralErrors(1L);
		}
        
        return false;
    }
	
	@Override
	protected void publishEvent(final UssdSrsGwEventCodes eventCode, final String additionalInfo) {
		if(additionalInfo != null)
			setAdditionalEventInfo(additionalInfo);
		publishReportEvent(eventCode, new UssdSrsGwEventHelper(UssdSrsGwSbb.this));
		setAdditionalEventInfo(null);
	}
	
	@Override
	protected void clearAlarms(String alarmType) {
		super.clearAlarms(String.format("%s.%s", localSessionTraceContext.getTracerName(), alarmType));
	}
	
	/**
	 * Create {@link DbQuerySbb} instance
	 *
	 * @return local interface to a given SBB instance
	 */
	DbQuerySbbLocalInterface getDbQuerySbb() throws Exception {
		if(isTraceable(FINEST))
			finest("getDbQuerySbb: entering");

		if (dbQuery == null) {
			if (getDbQuerySbbRelation().isEmpty())
				dbQuery = (DbQuerySbbLocalInterface) getDbQuerySbbRelation().create();
			else
				dbQuery = (DbQuerySbbLocalInterface) getDbQuerySbbRelation().iterator().next();
		}

		if (dbQuery == null)
			throw new CreateException("DbQuerySbb create method returned null result");

		if (isTraceable(FINEST))
			finest("getDbQuerySbb: exiting");

		return dbQuery;
	}
	
	/**
	 * Create {@link NumberNormalizerSbb} instance
	 * @return sbb instance regarding to given local interface
	 * @throws Exception
	 */
	NumberNormalizerSbbLocalInterface getNumberNormalizerSbb() throws Exception {
		if (isTraceable(FINEST))
			finest("getNumberNormalizer: entering");

		if (numberNormalizer == null) {
			if (getNumberNormalizerSbbRelation().isEmpty())
				numberNormalizer = (NumberNormalizerSbbLocalInterface) getNumberNormalizerSbbRelation().create();
			else
				numberNormalizer = (NumberNormalizerSbbLocalInterface) getNumberNormalizerSbbRelation().iterator().next();
		}
		
		if (numberNormalizer == null)
			throw new CreateException("NumberNormalizerSbb create method returned null result");

		if (isTraceable(FINEST))
			finest("getNumberNormalizer: exiting");

		return numberNormalizer;
	}
	
	@Override
	protected void cleanNonCmpFields() {
		super.cleanNonCmpFields();
		dbQuery = null;
		numberNormalizer = null;
	}
	
	/**
	 * Enforced by {@link com.nsn.ploc.feniks.slee.nsnbaselib.BaseSbb#getSbbName()} method
	 * 
	 * @return SBB object name
	 */
	@Override
    protected String getSbbName() {
        return UssdSrsGwSbb.class.getSimpleName();
    }

    /**
     * @slee.child-relation-method
     *   sbb-alias-ref="DbQuerySbb"
     *   default-priority="0"
     */
 	public abstract ChildRelation getDbQuerySbbRelation();
 	private DbQuerySbbLocalInterface dbQuery = null;
 	
 	/**
     * @slee.child-relation-method
     *   sbb-alias-ref="NumberNormalizerSbb"
     *   default-priority="0"
     */
 	public abstract ChildRelation getNumberNormalizerSbbRelation();
	private NumberNormalizerSbbLocalInterface numberNormalizer = null;
}
