package pl.com.polkomtel.drpu.slee.resources.srs.stubs;


/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public interface vasOperations
{
	/* constants */
	/* operations  */
	void TopUp(java.lang.String secretNr, java.lang.String msisdn, short type, java.lang.String originator, java.lang.String channel, java.lang.String xmlin, org.omg.CORBA.ShortHolder validationResult, org.omg.CORBA.IntHolder credit, org.omg.CORBA.StringHolder appProfile, org.omg.CORBA.StringHolder xmlout) throws pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccess,pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.SystemError;
	void checkService(org.omg.CORBA.ShortHolder available);
}
