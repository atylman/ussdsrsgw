package pl.com.polkomtel.drpu.slee.resources.srs.test.skeletons;

/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-02-20 15:38:09
 */

public interface vas
	extends vasOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
