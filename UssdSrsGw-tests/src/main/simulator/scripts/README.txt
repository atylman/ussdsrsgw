All E2E tests can be run with a single bash script:

	./run-tests.sh

which performs the following set of actions:
- configures Rhino server (UssdSrsGw service, SRS RA)
- starts SrsSimulator which is supposed to handle CORBA communication from SRS RA
- executes all 9 test scenarios
- verifies test results (EDRs, logs) and prints final results summary to console

Before any tests are started, the following prerequisites ought to be fulfilled:
- Rhino server is up and running

- CGIN RA entity "cginraUssdGwSim" for the simulated ITU TCAP stack has to be created and activated beforehand with the following properties:
	tcapsim.listen-addresses: localhost:10200
	tcapsim.remote-addresses: localhost:10100
	stack: tcapsim
	tcapsim.sctp-stack: tcp
	local-sccp-address: type=C7,ri=pcssn,pc=2,ssn=5
	tcapsim.gt-table: ${/}opt${/}opencloud${/}rhinoB${/}rhino-connectivity${/}cgin${/}tcapsim-gt-table.txt
	enabled-protocols: map
	
  For example with such kind of cmd:
  
	createraentity name=CGIN\ Unified\ RA\ with\ Nokia-Siemens\ Networks\ protocols,vendor=OpenCloud,version=1.5 cginraUssdGwSim local-sccp-address type=C7,ri=pcssn,pc=2,ssn=5 tcapsim.listen-addresses localhost:10200 tcapsim.remote-addresses localhost:10100 stack tcapsim enabled-protocols map tcapsim.sctp-stack tcp

- the entire USSD-SRS Gateway solution (both service and RA) is successfully deployed, map.ra.entity.name property in ant script has to be changed to "cginraUssdGwSim" value

- all environment specific variables included in tests.properties file are up-to-date  

Test cases:
1) *123#
2) #123#
3) #123#0123456789012345#
4) *500*0000000000000000#
5) *123*0000000000000000#
6) *123*0000000000000000*48600111111#
7) *123*0000000000000001#
8) *123*0000000000000099#
9) SRS RA activation with invalid corbaloc 