package pl.com.polkomtel.drpu.slee.resources.srs;

public interface SrsResourceAdaptorTestConst {
	String VALID_REMOTE_CORBALOC = "corbaloc:iiop:1.2@10.10.10.30:50060/%FFvca%00vas";
	String INVALID_REMOTE_CORBALOC = "corba:iiop@10.10.10.30:50060/%FFvca%00vas";
}
