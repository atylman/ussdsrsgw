package pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage;

/**
 * Generated from IDL exception "NoAccess".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public final class NoAccess
	extends org.omg.CORBA.UserException
{
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	public NoAccess()
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccessHelper.id());
	}

	public java.lang.String description = "";
	public NoAccess(java.lang.String _reason,java.lang.String description)
	{
		super(_reason);
		this.description = description;
	}
	public NoAccess(java.lang.String description)
	{
		super(pl.com.polkomtel.drpu.slee.resources.srs.stubs.vasPackage.NoAccessHelper.id());
		this.description = description;
	}
}
