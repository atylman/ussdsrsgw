package pl.com.polkomtel.drpu.slee.resources.srs;

/**
 * An exception thrown when any kind of CORBA communication error occurs. 
 * 
 * @author adam.tylman
 */
public class ClientRequestException extends Exception {
	private static final long serialVersionUID = 8231734215870405105L;

	/**
	 * Default constructor
	 */
	public ClientRequestException() {
		super();
	}
	
	/**
	 * Constructor which passes an informational message
	 * @param message a message to pass
	 */
	public ClientRequestException(String message) {
		super(message);
	}

	/**
	 * Constructor which passes the cause of exception
	 * @param cause a cause to pass
	 */
	public ClientRequestException(Throwable cause) {
		super(cause);
	}
}
