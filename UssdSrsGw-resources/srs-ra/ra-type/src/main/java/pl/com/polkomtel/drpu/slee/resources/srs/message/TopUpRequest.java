package pl.com.polkomtel.drpu.slee.resources.srs.message;

import java.io.Serializable;

/**
 * Represents CORBA TopUp request.
 * 
 * @author adam.tylman
 */
public class TopUpRequest implements Serializable {
	private static final long serialVersionUID = 301225201391730806L;

	private String secretNr;
	private String msisdn;
	private short type;
	private String originator;
	private String channel;
	private String xmlin;

	/**
	 * Primary constructor.
	 * 
	 * @param secretNr voucher secret number as printed on card
	 * @param msisdn MSISDN to top up
	 * @param type type of subscriber if known
	 * @param originator originator (can be MSISDN, or another identifier)
	 * @param channel channel information (IVR, USSD, etc.)
	 * @param xmlin optional XML payload for customer specific extensions
	 */
	public TopUpRequest(final String secretNr, final String msisdn, final short type, final String originator, final String channel, final String xmlin) {
		super();
		this.secretNr = secretNr;
		this.msisdn = msisdn;
		this.type = type;
		this.originator = originator;
		this.channel = channel;
		this.xmlin = xmlin;
	}

	/**
	 * @return the secretNr
	 */
	public String getSecretNr() {
		return secretNr;
	}

	/**
	 * @param secretNr the secretNr to set
	 */
	public void setSecretNr(final String secretNr) {
		this.secretNr = secretNr;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(final String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the type
	 */
	public short getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final short type) {
		this.type = type;
	}

	/**
	 * @return the originator
	 */
	public String getOriginator() {
		return originator;
	}

	/**
	 * @param originator the originator to set
	 */
	public void setOriginator(final String originator) {
		this.originator = originator;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(final String channel) {
		this.channel = channel;
	}

	/**
	 * @return the xmlin
	 */
	public String getXmlin() {
		return xmlin;
	}

	/**
	 * @param xmlin the xmlin to set
	 */
	public void setXmlin(final String xmlin) {
		this.xmlin = xmlin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder("TopUpRequest [");
		if (secretNr != null) {
			builder.append("secretNr=");
			builder.append(secretNr);
			builder.append(", ");
		}
		if (msisdn != null) {
			builder.append("msisdn=");
			builder.append(msisdn);
			builder.append(", ");
		}
		builder.append("type=");
		builder.append(type);
		builder.append(", ");
		if (originator != null) {
			builder.append("originator=");
			builder.append(originator);
			builder.append(", ");
		}
		if (channel != null) {
			builder.append("channel=");
			builder.append(channel);
			builder.append(", ");
		}
		if (xmlin != null) {
			builder.append("xmlin=");
			builder.append(xmlin);
		}
		builder.append(']');
		return builder.toString();
	}
}
