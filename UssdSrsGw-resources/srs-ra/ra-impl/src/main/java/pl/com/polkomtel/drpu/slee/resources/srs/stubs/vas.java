package pl.com.polkomtel.drpu.slee.resources.srs.stubs;

/**
 * Generated from IDL interface "vas".
 *
 * @author JacORB IDL compiler V 3.0, 22-May-2012
 * @version generated at 2015-01-21 13:05:45
 */

public interface vas
	extends vasOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
