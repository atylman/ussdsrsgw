package pl.com.polkomtel.drpu.slee.resources.srs;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import java.util.Properties;

import javax.slee.CreateException;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;
import javax.slee.resource.ConfigProperties;
import javax.slee.resource.ConfigProperties.Property;
import javax.slee.resource.ResourceAdaptorContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.Stubber;
import org.omg.CORBA.ORB;
import org.omg.CORBA.TRANSIENT;

import pl.com.polkomtel.drpu.slee.resources.srs.SrsResourceAdaptor.State;
import pl.com.polkomtel.drpu.slee.resources.srs.stubs.vas;

public abstract class BaseResourceAdaptorTest implements SrsResourceAdaptorTestConst {
	
	protected SrsResourceAdaptor raSpy;
	protected AlarmFacility alarmFacilityMock;
	
	@Rule
    public TestName testName = new TestName();
	
	@Before
    public final void setUp() throws CreateException, Exception {
		System.out.println(String.format("\nTest: %s", testName.getMethodName()));
		raSpy = spy(new SrsResourceAdaptor());
		mockSrsRaTracer(raSpy);
	}
	
	protected void mockSrsRaTracer(SrsResourceAdaptor ra) {
		Tracer mockedTracer = mock(Tracer.class);
		mockTracerGuards(mockedTracer);
		mockTracerAnswear(mockedTracer);
		when(mockedTracer.getTracerName()).thenReturn("plk.srsra");
		
		ResourceAdaptorContext mockedContext = mock(ResourceAdaptorContext.class);
		when(mockedContext.getTracer(anyString())).thenReturn(mockedTracer);
		
		alarmFacilityMock = mock(AlarmFacility.class);
		when(mockedContext.getAlarmFacility()).thenReturn(alarmFacilityMock);
		
		ra.setResourceAdaptorContext(mockedContext);
	}
	
	protected void mockTracerGuards(Tracer mockedTracer) {
		when(mockedTracer.isFinestEnabled()).thenReturn(true);
		when(mockedTracer.isFinerEnabled()).thenReturn(true);
		when(mockedTracer.isFineEnabled()).thenReturn(true);
		when(mockedTracer.isConfigEnabled()).thenReturn(true);
		when(mockedTracer.isInfoEnabled()).thenReturn(true);
	}
	
	protected void mockTracerAnswear(Tracer mockedTracer) {
		Stubber doAnswerStub = doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock mock) throws Throwable {
				StringBuilder log = new StringBuilder();
				log.append(mock.getMethod().getName());
				log.append(" -> ");

				Object[] arguments = mock.getArguments();
				for (Object argument : arguments) {
					if (argument != null) {
						log.append(argument).append(" ");
					}
				}
				System.out.println(log);
				return null;
			}
		});

		doAnswerStub.when(mockedTracer).trace(any(TraceLevel.class), anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).finest(anyString());
		doAnswerStub.when(mockedTracer).finest(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).finer(anyString());
		doAnswerStub.when(mockedTracer).finer(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).fine(anyString());
		doAnswerStub.when(mockedTracer).fine(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).info(anyString());
		doAnswerStub.when(mockedTracer).info(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).warning(anyString());
		doAnswerStub.when(mockedTracer).warning(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).severe(anyString());
		doAnswerStub.when(mockedTracer).severe(anyString(), any(Throwable.class));
	}
	
	protected void assertValidationExceptionThrown(Property[] properties, String exceptionMsg) throws Exception {
		try {
			raSpy.raConfigure(new ConfigProperties(properties));
			fail("RuntimeException should be thrown due to invalid config: " + properties);
		} catch(Exception ex) {
			assertThat(ex)
					.isInstanceOf(RuntimeException.class);
			assertTrue("RuntimeException seems to have wrong description", ex.getMessage().contains(exceptionMsg));
		}

		verifyPrivate(raSpy, never()).invoke("setState", eq(State.INACTIVE));
	}
	
	protected void mockCorbaStubs(boolean withOrbMalfunction) {
		vas mockedServer = mock(vas.class);
		ORB mockedOrb = mock(ORB.class);
		mockStatic(ORB.class);
		
		if(withOrbMalfunction)
			when(mockedOrb.string_to_object(anyString())).thenThrow(new TRANSIENT("Retries exceeded, couldn't reconnect"));
		else
			when(mockedOrb.string_to_object(anyString())).thenReturn((org.omg.CORBA.Object) mockedServer);
		
		when(ORB.init(any(String[].class), any(Properties.class))).thenReturn(mockedOrb);
	}
	
	protected void assertNoAlarmsRaised() {
		verify(alarmFacilityMock, never()).raiseAlarm(anyString(), anyString(), any(AlarmLevel.class), anyString());
		verify(alarmFacilityMock, never()).raiseAlarm(anyString(), anyString(), any(AlarmLevel.class), anyString(), any(Throwable.class));
	}
}
